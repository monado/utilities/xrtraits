#!/usr/bin/python3
#
# Copyright 2018, Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0
#
# Author(s):    Ryan Pavlik <ryan.pavlik@collabora.com>
#
# Purpose:      This file generates an IWYU mapping file to
#               convert C-style standard includes like <stdint.h>
#               to their C++ equivalent (eg <cstdint>)

INPUT = [
    "stdint",
    "stddef",
    "string"
]

print("[")

mapping_lines = [ """    { "include": [ "<%s.h>", "public", "<c%s>", "public" ] }""" % (name, name)
    for name in INPUT]

print(",\n".join(mapping_lines))

print("]")
