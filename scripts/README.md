# Scripts

## From OpenXR-SDK

From the 0.90.0 release generation, commit 5ca8a38cb7a65fc0ffcea88f565f27465d3b5be8.

from `specification/scripts`:

- `reg.py`
- `generator.py`

from `src/scripts`:

- `automatic_source_generator.py`
- `genxr_utils.py` is extracted from `src_genxr.py`
