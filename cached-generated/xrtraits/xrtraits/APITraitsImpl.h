/*
** Copyright (c) 2017-2019 The Khronos Group Inc.
** Copyright 2018-2019, Collabora, Ltd.
**
** SPDX-License-Identifier: Apache-2.0
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

/*
** This header is generated from the Khronos OpenXR XML API Registry.
**
*/

// *********** THIS FILE IS GENERATED - DO NOT EDIT ***********
//     See structure_traits_generator.py for modifications
// ************************************************************

//
// Author: Ryan Pavlik <ryan.pavlik@collabora.com>
//

#pragma once

#pragma once

// IWYU pragma: private, include "xrtraits/traits/APITraits.h"

#include "xrtraits/traits/APITraitsFwd.h"

namespace xrtraits {
namespace traits {
    namespace detail {

        // ---- Core 1_0

        template <> constexpr bool is_xr_tagged_type_impl_v<XrApiLayerProperties> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrApiLayerProperties> = "XrApiLayerProperties";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrApiLayerProperties> = XR_TYPE_API_LAYER_PROPERTIES;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_API_LAYER_PROPERTIES> = "XrApiLayerProperties";
        template <> constexpr bool has_xr_type_tag_impl_v<XrApiLayerProperties> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrExtensionProperties> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrExtensionProperties> = "XrExtensionProperties";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrExtensionProperties> = XR_TYPE_EXTENSION_PROPERTIES;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_EXTENSION_PROPERTIES> = "XrExtensionProperties";
        template <> constexpr bool has_xr_type_tag_impl_v<XrExtensionProperties> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrInstanceCreateInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrInstanceCreateInfo> = "XrInstanceCreateInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrInstanceCreateInfo> = XR_TYPE_INSTANCE_CREATE_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_INSTANCE_CREATE_INFO> = "XrInstanceCreateInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrInstanceCreateInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrInstanceProperties> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrInstanceProperties> = "XrInstanceProperties";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrInstanceProperties> = XR_TYPE_INSTANCE_PROPERTIES;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_INSTANCE_PROPERTIES> = "XrInstanceProperties";
        template <> constexpr bool has_xr_type_tag_impl_v<XrInstanceProperties> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrEventDataBuffer> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrEventDataBuffer> = "XrEventDataBuffer";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrEventDataBuffer> = XR_TYPE_EVENT_DATA_BUFFER;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_BUFFER> = "XrEventDataBuffer";
        template <> constexpr bool has_xr_type_tag_impl_v<XrEventDataBuffer> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrSystemGetInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSystemGetInfo> = "XrSystemGetInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSystemGetInfo> = XR_TYPE_SYSTEM_GET_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SYSTEM_GET_INFO> = "XrSystemGetInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSystemGetInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrSystemProperties> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSystemProperties> = "XrSystemProperties";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSystemProperties> = XR_TYPE_SYSTEM_PROPERTIES;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SYSTEM_PROPERTIES> = "XrSystemProperties";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSystemProperties> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrSessionCreateInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSessionCreateInfo> = "XrSessionCreateInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSessionCreateInfo> = XR_TYPE_SESSION_CREATE_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SESSION_CREATE_INFO> = "XrSessionCreateInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSessionCreateInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrSpaceVelocity> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSpaceVelocity> = "XrSpaceVelocity";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSpaceVelocity> = XR_TYPE_SPACE_VELOCITY;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SPACE_VELOCITY> = "XrSpaceVelocity";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSpaceVelocity> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrReferenceSpaceCreateInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrReferenceSpaceCreateInfo> = "XrReferenceSpaceCreateInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrReferenceSpaceCreateInfo> = XR_TYPE_REFERENCE_SPACE_CREATE_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_REFERENCE_SPACE_CREATE_INFO> = "XrReferenceSpaceCreateInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrReferenceSpaceCreateInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrActionSpaceCreateInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrActionSpaceCreateInfo> = "XrActionSpaceCreateInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrActionSpaceCreateInfo> = XR_TYPE_ACTION_SPACE_CREATE_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_SPACE_CREATE_INFO> = "XrActionSpaceCreateInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrActionSpaceCreateInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrSpaceLocation> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSpaceLocation> = "XrSpaceLocation";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSpaceLocation> = XR_TYPE_SPACE_LOCATION;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SPACE_LOCATION> = "XrSpaceLocation";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSpaceLocation> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrViewConfigurationProperties> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrViewConfigurationProperties> = "XrViewConfigurationProperties";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrViewConfigurationProperties> = XR_TYPE_VIEW_CONFIGURATION_PROPERTIES;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_VIEW_CONFIGURATION_PROPERTIES> = "XrViewConfigurationProperties";
        template <> constexpr bool has_xr_type_tag_impl_v<XrViewConfigurationProperties> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrViewConfigurationView> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrViewConfigurationView> = "XrViewConfigurationView";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrViewConfigurationView> = XR_TYPE_VIEW_CONFIGURATION_VIEW;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_VIEW_CONFIGURATION_VIEW> = "XrViewConfigurationView";
        template <> constexpr bool has_xr_type_tag_impl_v<XrViewConfigurationView> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrSwapchainCreateInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSwapchainCreateInfo> = "XrSwapchainCreateInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSwapchainCreateInfo> = XR_TYPE_SWAPCHAIN_CREATE_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_CREATE_INFO> = "XrSwapchainCreateInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSwapchainCreateInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrSwapchainImageBaseHeader> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSwapchainImageBaseHeader> = "XrSwapchainImageBaseHeader";

        template <> constexpr bool is_xr_tagged_type_impl_v<XrSwapchainImageAcquireInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSwapchainImageAcquireInfo> = "XrSwapchainImageAcquireInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSwapchainImageAcquireInfo> = XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO> = "XrSwapchainImageAcquireInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSwapchainImageAcquireInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrSwapchainImageWaitInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSwapchainImageWaitInfo> = "XrSwapchainImageWaitInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSwapchainImageWaitInfo> = XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO> = "XrSwapchainImageWaitInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSwapchainImageWaitInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrSwapchainImageReleaseInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSwapchainImageReleaseInfo> = "XrSwapchainImageReleaseInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSwapchainImageReleaseInfo> = XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO> = "XrSwapchainImageReleaseInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSwapchainImageReleaseInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrSessionBeginInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSessionBeginInfo> = "XrSessionBeginInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSessionBeginInfo> = XR_TYPE_SESSION_BEGIN_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SESSION_BEGIN_INFO> = "XrSessionBeginInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSessionBeginInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrFrameWaitInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrFrameWaitInfo> = "XrFrameWaitInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrFrameWaitInfo> = XR_TYPE_FRAME_WAIT_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_FRAME_WAIT_INFO> = "XrFrameWaitInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrFrameWaitInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrFrameState> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrFrameState> = "XrFrameState";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrFrameState> = XR_TYPE_FRAME_STATE;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_FRAME_STATE> = "XrFrameState";
        template <> constexpr bool has_xr_type_tag_impl_v<XrFrameState> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrFrameBeginInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrFrameBeginInfo> = "XrFrameBeginInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrFrameBeginInfo> = XR_TYPE_FRAME_BEGIN_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_FRAME_BEGIN_INFO> = "XrFrameBeginInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrFrameBeginInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrCompositionLayerBaseHeader> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrCompositionLayerBaseHeader> = "XrCompositionLayerBaseHeader";

        template <> constexpr bool is_xr_tagged_type_impl_v<XrFrameEndInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrFrameEndInfo> = "XrFrameEndInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrFrameEndInfo> = XR_TYPE_FRAME_END_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_FRAME_END_INFO> = "XrFrameEndInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrFrameEndInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrViewLocateInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrViewLocateInfo> = "XrViewLocateInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrViewLocateInfo> = XR_TYPE_VIEW_LOCATE_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_VIEW_LOCATE_INFO> = "XrViewLocateInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrViewLocateInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrViewState> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrViewState> = "XrViewState";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrViewState> = XR_TYPE_VIEW_STATE;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_VIEW_STATE> = "XrViewState";
        template <> constexpr bool has_xr_type_tag_impl_v<XrViewState> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrView> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrView> = "XrView";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrView> = XR_TYPE_VIEW;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_VIEW> = "XrView";
        template <> constexpr bool has_xr_type_tag_impl_v<XrView> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrActionSetCreateInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrActionSetCreateInfo> = "XrActionSetCreateInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrActionSetCreateInfo> = XR_TYPE_ACTION_SET_CREATE_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_SET_CREATE_INFO> = "XrActionSetCreateInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrActionSetCreateInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrActionCreateInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrActionCreateInfo> = "XrActionCreateInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrActionCreateInfo> = XR_TYPE_ACTION_CREATE_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_CREATE_INFO> = "XrActionCreateInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrActionCreateInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrInteractionProfileSuggestedBinding> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrInteractionProfileSuggestedBinding> = "XrInteractionProfileSuggestedBinding";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrInteractionProfileSuggestedBinding> = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING> = "XrInteractionProfileSuggestedBinding";
        template <> constexpr bool has_xr_type_tag_impl_v<XrInteractionProfileSuggestedBinding> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrSessionActionSetsAttachInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSessionActionSetsAttachInfo> = "XrSessionActionSetsAttachInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSessionActionSetsAttachInfo> = XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO> = "XrSessionActionSetsAttachInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSessionActionSetsAttachInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrInteractionProfileState> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrInteractionProfileState> = "XrInteractionProfileState";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrInteractionProfileState> = XR_TYPE_INTERACTION_PROFILE_STATE;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_INTERACTION_PROFILE_STATE> = "XrInteractionProfileState";
        template <> constexpr bool has_xr_type_tag_impl_v<XrInteractionProfileState> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrActionStateGetInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrActionStateGetInfo> = "XrActionStateGetInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrActionStateGetInfo> = XR_TYPE_ACTION_STATE_GET_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_STATE_GET_INFO> = "XrActionStateGetInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrActionStateGetInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrActionStateBoolean> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrActionStateBoolean> = "XrActionStateBoolean";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrActionStateBoolean> = XR_TYPE_ACTION_STATE_BOOLEAN;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_STATE_BOOLEAN> = "XrActionStateBoolean";
        template <> constexpr bool has_xr_type_tag_impl_v<XrActionStateBoolean> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrActionStateFloat> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrActionStateFloat> = "XrActionStateFloat";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrActionStateFloat> = XR_TYPE_ACTION_STATE_FLOAT;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_STATE_FLOAT> = "XrActionStateFloat";
        template <> constexpr bool has_xr_type_tag_impl_v<XrActionStateFloat> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrActionStateVector2f> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrActionStateVector2f> = "XrActionStateVector2f";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrActionStateVector2f> = XR_TYPE_ACTION_STATE_VECTOR2F;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_STATE_VECTOR2F> = "XrActionStateVector2f";
        template <> constexpr bool has_xr_type_tag_impl_v<XrActionStateVector2f> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrActionStatePose> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrActionStatePose> = "XrActionStatePose";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrActionStatePose> = XR_TYPE_ACTION_STATE_POSE;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_STATE_POSE> = "XrActionStatePose";
        template <> constexpr bool has_xr_type_tag_impl_v<XrActionStatePose> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrActionsSyncInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrActionsSyncInfo> = "XrActionsSyncInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrActionsSyncInfo> = XR_TYPE_ACTIONS_SYNC_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_ACTIONS_SYNC_INFO> = "XrActionsSyncInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrActionsSyncInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrBoundSourcesForActionEnumerateInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrBoundSourcesForActionEnumerateInfo> = "XrBoundSourcesForActionEnumerateInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrBoundSourcesForActionEnumerateInfo> = XR_TYPE_BOUND_SOURCES_FOR_ACTION_ENUMERATE_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_BOUND_SOURCES_FOR_ACTION_ENUMERATE_INFO> = "XrBoundSourcesForActionEnumerateInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrBoundSourcesForActionEnumerateInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrInputSourceLocalizedNameGetInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrInputSourceLocalizedNameGetInfo> = "XrInputSourceLocalizedNameGetInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrInputSourceLocalizedNameGetInfo> = XR_TYPE_INPUT_SOURCE_LOCALIZED_NAME_GET_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_INPUT_SOURCE_LOCALIZED_NAME_GET_INFO> = "XrInputSourceLocalizedNameGetInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrInputSourceLocalizedNameGetInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrHapticActionInfo> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrHapticActionInfo> = "XrHapticActionInfo";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrHapticActionInfo> = XR_TYPE_HAPTIC_ACTION_INFO;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_HAPTIC_ACTION_INFO> = "XrHapticActionInfo";
        template <> constexpr bool has_xr_type_tag_impl_v<XrHapticActionInfo> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrHapticBaseHeader> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrHapticBaseHeader> = "XrHapticBaseHeader";

        template <> constexpr bool is_xr_tagged_type_impl_v<XrBaseInStructure> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrBaseInStructure> = "XrBaseInStructure";

        template <> constexpr bool is_xr_tagged_type_impl_v<XrBaseOutStructure> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrBaseOutStructure> = "XrBaseOutStructure";

        template <> constexpr bool is_xr_tagged_type_impl_v<XrCompositionLayerProjectionView> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrCompositionLayerProjectionView> = "XrCompositionLayerProjectionView";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrCompositionLayerProjectionView> = XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW> = "XrCompositionLayerProjectionView";
        template <> constexpr bool has_xr_type_tag_impl_v<XrCompositionLayerProjectionView> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrCompositionLayerProjection> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrCompositionLayerProjection> = "XrCompositionLayerProjection";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrCompositionLayerProjection> = XR_TYPE_COMPOSITION_LAYER_PROJECTION;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_COMPOSITION_LAYER_PROJECTION> = "XrCompositionLayerProjection";
        template <> constexpr bool has_xr_type_tag_impl_v<XrCompositionLayerProjection> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrCompositionLayerQuad> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrCompositionLayerQuad> = "XrCompositionLayerQuad";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrCompositionLayerQuad> = XR_TYPE_COMPOSITION_LAYER_QUAD;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_COMPOSITION_LAYER_QUAD> = "XrCompositionLayerQuad";
        template <> constexpr bool has_xr_type_tag_impl_v<XrCompositionLayerQuad> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrEventDataBaseHeader> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrEventDataBaseHeader> = "XrEventDataBaseHeader";

        template <> constexpr bool is_xr_tagged_type_impl_v<XrEventDataEventsLost> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrEventDataEventsLost> = "XrEventDataEventsLost";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrEventDataEventsLost> = XR_TYPE_EVENT_DATA_EVENTS_LOST;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_EVENTS_LOST> = "XrEventDataEventsLost";
        template <> constexpr bool has_xr_type_tag_impl_v<XrEventDataEventsLost> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrEventDataInstanceLossPending> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrEventDataInstanceLossPending> = "XrEventDataInstanceLossPending";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrEventDataInstanceLossPending> = XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING> = "XrEventDataInstanceLossPending";
        template <> constexpr bool has_xr_type_tag_impl_v<XrEventDataInstanceLossPending> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrEventDataSessionStateChanged> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrEventDataSessionStateChanged> = "XrEventDataSessionStateChanged";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrEventDataSessionStateChanged> = XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED> = "XrEventDataSessionStateChanged";
        template <> constexpr bool has_xr_type_tag_impl_v<XrEventDataSessionStateChanged> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrEventDataReferenceSpaceChangePending> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrEventDataReferenceSpaceChangePending> = "XrEventDataReferenceSpaceChangePending";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrEventDataReferenceSpaceChangePending> = XR_TYPE_EVENT_DATA_REFERENCE_SPACE_CHANGE_PENDING;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_REFERENCE_SPACE_CHANGE_PENDING> = "XrEventDataReferenceSpaceChangePending";
        template <> constexpr bool has_xr_type_tag_impl_v<XrEventDataReferenceSpaceChangePending> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrEventDataInteractionProfileChanged> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrEventDataInteractionProfileChanged> = "XrEventDataInteractionProfileChanged";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrEventDataInteractionProfileChanged> = XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED> = "XrEventDataInteractionProfileChanged";
        template <> constexpr bool has_xr_type_tag_impl_v<XrEventDataInteractionProfileChanged> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrHapticVibration> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrHapticVibration> = "XrHapticVibration";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrHapticVibration> = XR_TYPE_HAPTIC_VIBRATION;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_HAPTIC_VIBRATION> = "XrHapticVibration";
        template <> constexpr bool has_xr_type_tag_impl_v<XrHapticVibration> = true;

        // ---- XR_KHR_composition_layer_cube extension

        template <> constexpr bool is_xr_tagged_type_impl_v<XrCompositionLayerCubeKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrCompositionLayerCubeKHR> = "XrCompositionLayerCubeKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrCompositionLayerCubeKHR> = XR_TYPE_COMPOSITION_LAYER_CUBE_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_COMPOSITION_LAYER_CUBE_KHR> = "XrCompositionLayerCubeKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrCompositionLayerCubeKHR> = true;

        // ---- XR_KHR_android_create_instance extension

#if defined(XR_USE_PLATFORM_ANDROID)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrInstanceCreateInfoAndroidKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrInstanceCreateInfoAndroidKHR> = "XrInstanceCreateInfoAndroidKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrInstanceCreateInfoAndroidKHR> = XR_TYPE_INSTANCE_CREATE_INFO_ANDROID_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_INSTANCE_CREATE_INFO_ANDROID_KHR> = "XrInstanceCreateInfoAndroidKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrInstanceCreateInfoAndroidKHR> = true;
#endif // defined(XR_USE_PLATFORM_ANDROID)

        // ---- XR_KHR_composition_layer_depth extension

        template <> constexpr bool is_xr_tagged_type_impl_v<XrCompositionLayerDepthInfoKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrCompositionLayerDepthInfoKHR> = "XrCompositionLayerDepthInfoKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrCompositionLayerDepthInfoKHR> = XR_TYPE_COMPOSITION_LAYER_DEPTH_INFO_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_COMPOSITION_LAYER_DEPTH_INFO_KHR> = "XrCompositionLayerDepthInfoKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrCompositionLayerDepthInfoKHR> = true;

        // ---- XR_KHR_vulkan_swapchain_format_list extension

#if defined(XR_USE_GRAPHICS_API_VULKAN)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrVulkanSwapchainFormatListCreateInfoKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrVulkanSwapchainFormatListCreateInfoKHR> = "XrVulkanSwapchainFormatListCreateInfoKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrVulkanSwapchainFormatListCreateInfoKHR> = XR_TYPE_VULKAN_SWAPCHAIN_FORMAT_LIST_CREATE_INFO_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_VULKAN_SWAPCHAIN_FORMAT_LIST_CREATE_INFO_KHR> = "XrVulkanSwapchainFormatListCreateInfoKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrVulkanSwapchainFormatListCreateInfoKHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

        // ---- XR_KHR_composition_layer_cylinder extension

        template <> constexpr bool is_xr_tagged_type_impl_v<XrCompositionLayerCylinderKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrCompositionLayerCylinderKHR> = "XrCompositionLayerCylinderKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrCompositionLayerCylinderKHR> = XR_TYPE_COMPOSITION_LAYER_CYLINDER_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_COMPOSITION_LAYER_CYLINDER_KHR> = "XrCompositionLayerCylinderKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrCompositionLayerCylinderKHR> = true;

        // ---- XR_KHR_composition_layer_equirect extension

        template <> constexpr bool is_xr_tagged_type_impl_v<XrCompositionLayerEquirectKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrCompositionLayerEquirectKHR> = "XrCompositionLayerEquirectKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrCompositionLayerEquirectKHR> = XR_TYPE_COMPOSITION_LAYER_EQUIRECT_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_COMPOSITION_LAYER_EQUIRECT_KHR> = "XrCompositionLayerEquirectKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrCompositionLayerEquirectKHR> = true;

        // ---- XR_KHR_opengl_enable extension

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WIN32)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrGraphicsBindingOpenGLWin32KHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrGraphicsBindingOpenGLWin32KHR> = "XrGraphicsBindingOpenGLWin32KHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrGraphicsBindingOpenGLWin32KHR> = XR_TYPE_GRAPHICS_BINDING_OPENGL_WIN32_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_OPENGL_WIN32_KHR> = "XrGraphicsBindingOpenGLWin32KHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrGraphicsBindingOpenGLWin32KHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WIN32)

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XLIB)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrGraphicsBindingOpenGLXlibKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrGraphicsBindingOpenGLXlibKHR> = "XrGraphicsBindingOpenGLXlibKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrGraphicsBindingOpenGLXlibKHR> = XR_TYPE_GRAPHICS_BINDING_OPENGL_XLIB_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_OPENGL_XLIB_KHR> = "XrGraphicsBindingOpenGLXlibKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrGraphicsBindingOpenGLXlibKHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XLIB)

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XCB)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrGraphicsBindingOpenGLXcbKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrGraphicsBindingOpenGLXcbKHR> = "XrGraphicsBindingOpenGLXcbKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrGraphicsBindingOpenGLXcbKHR> = XR_TYPE_GRAPHICS_BINDING_OPENGL_XCB_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_OPENGL_XCB_KHR> = "XrGraphicsBindingOpenGLXcbKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrGraphicsBindingOpenGLXcbKHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XCB)

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WAYLAND)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrGraphicsBindingOpenGLWaylandKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrGraphicsBindingOpenGLWaylandKHR> = "XrGraphicsBindingOpenGLWaylandKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrGraphicsBindingOpenGLWaylandKHR> = XR_TYPE_GRAPHICS_BINDING_OPENGL_WAYLAND_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_OPENGL_WAYLAND_KHR> = "XrGraphicsBindingOpenGLWaylandKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrGraphicsBindingOpenGLWaylandKHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WAYLAND)

#if defined(XR_USE_GRAPHICS_API_OPENGL)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrSwapchainImageOpenGLKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSwapchainImageOpenGLKHR> = "XrSwapchainImageOpenGLKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSwapchainImageOpenGLKHR> = XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_KHR> = "XrSwapchainImageOpenGLKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSwapchainImageOpenGLKHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL)

#if defined(XR_USE_GRAPHICS_API_OPENGL)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrGraphicsRequirementsOpenGLKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrGraphicsRequirementsOpenGLKHR> = "XrGraphicsRequirementsOpenGLKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrGraphicsRequirementsOpenGLKHR> = XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_KHR> = "XrGraphicsRequirementsOpenGLKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrGraphicsRequirementsOpenGLKHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL)

        // ---- XR_KHR_opengl_es_enable extension

#if defined(XR_USE_GRAPHICS_API_OPENGL_ES) && defined(XR_USE_PLATFORM_ANDROID)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrGraphicsBindingOpenGLESAndroidKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrGraphicsBindingOpenGLESAndroidKHR> = "XrGraphicsBindingOpenGLESAndroidKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrGraphicsBindingOpenGLESAndroidKHR> = XR_TYPE_GRAPHICS_BINDING_OPENGL_ES_ANDROID_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_OPENGL_ES_ANDROID_KHR> = "XrGraphicsBindingOpenGLESAndroidKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrGraphicsBindingOpenGLESAndroidKHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL_ES) && defined(XR_USE_PLATFORM_ANDROID)

#if defined(XR_USE_GRAPHICS_API_OPENGL_ES)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrSwapchainImageOpenGLESKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSwapchainImageOpenGLESKHR> = "XrSwapchainImageOpenGLESKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSwapchainImageOpenGLESKHR> = XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_ES_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_ES_KHR> = "XrSwapchainImageOpenGLESKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSwapchainImageOpenGLESKHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL_ES)

#if defined(XR_USE_GRAPHICS_API_OPENGL_ES)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrGraphicsRequirementsOpenGLESKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrGraphicsRequirementsOpenGLESKHR> = "XrGraphicsRequirementsOpenGLESKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrGraphicsRequirementsOpenGLESKHR> = XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_ES_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_ES_KHR> = "XrGraphicsRequirementsOpenGLESKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrGraphicsRequirementsOpenGLESKHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL_ES)

        // ---- XR_KHR_vulkan_enable extension

#if defined(XR_USE_GRAPHICS_API_VULKAN)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrGraphicsBindingVulkanKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrGraphicsBindingVulkanKHR> = "XrGraphicsBindingVulkanKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrGraphicsBindingVulkanKHR> = XR_TYPE_GRAPHICS_BINDING_VULKAN_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_VULKAN_KHR> = "XrGraphicsBindingVulkanKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrGraphicsBindingVulkanKHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

#if defined(XR_USE_GRAPHICS_API_VULKAN)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrSwapchainImageVulkanKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSwapchainImageVulkanKHR> = "XrSwapchainImageVulkanKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSwapchainImageVulkanKHR> = XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR> = "XrSwapchainImageVulkanKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSwapchainImageVulkanKHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

#if defined(XR_USE_GRAPHICS_API_VULKAN)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrGraphicsRequirementsVulkanKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrGraphicsRequirementsVulkanKHR> = "XrGraphicsRequirementsVulkanKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrGraphicsRequirementsVulkanKHR> = XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR> = "XrGraphicsRequirementsVulkanKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrGraphicsRequirementsVulkanKHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

        // ---- XR_KHR_D3D11_enable extension

#if defined(XR_USE_GRAPHICS_API_D3D11)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrGraphicsBindingD3D11KHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrGraphicsBindingD3D11KHR> = "XrGraphicsBindingD3D11KHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrGraphicsBindingD3D11KHR> = XR_TYPE_GRAPHICS_BINDING_D3D11_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_D3D11_KHR> = "XrGraphicsBindingD3D11KHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrGraphicsBindingD3D11KHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_D3D11)

#if defined(XR_USE_GRAPHICS_API_D3D11)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrSwapchainImageD3D11KHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSwapchainImageD3D11KHR> = "XrSwapchainImageD3D11KHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSwapchainImageD3D11KHR> = XR_TYPE_SWAPCHAIN_IMAGE_D3D11_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_D3D11_KHR> = "XrSwapchainImageD3D11KHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSwapchainImageD3D11KHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_D3D11)

#if defined(XR_USE_GRAPHICS_API_D3D11)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrGraphicsRequirementsD3D11KHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrGraphicsRequirementsD3D11KHR> = "XrGraphicsRequirementsD3D11KHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrGraphicsRequirementsD3D11KHR> = XR_TYPE_GRAPHICS_REQUIREMENTS_D3D11_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_REQUIREMENTS_D3D11_KHR> = "XrGraphicsRequirementsD3D11KHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrGraphicsRequirementsD3D11KHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_D3D11)

        // ---- XR_KHR_D3D12_enable extension

#if defined(XR_USE_GRAPHICS_API_D3D12)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrGraphicsBindingD3D12KHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrGraphicsBindingD3D12KHR> = "XrGraphicsBindingD3D12KHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrGraphicsBindingD3D12KHR> = XR_TYPE_GRAPHICS_BINDING_D3D12_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_D3D12_KHR> = "XrGraphicsBindingD3D12KHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrGraphicsBindingD3D12KHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_D3D12)

#if defined(XR_USE_GRAPHICS_API_D3D12)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrSwapchainImageD3D12KHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSwapchainImageD3D12KHR> = "XrSwapchainImageD3D12KHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSwapchainImageD3D12KHR> = XR_TYPE_SWAPCHAIN_IMAGE_D3D12_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_D3D12_KHR> = "XrSwapchainImageD3D12KHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSwapchainImageD3D12KHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_D3D12)

#if defined(XR_USE_GRAPHICS_API_D3D12)
        template <> constexpr bool is_xr_tagged_type_impl_v<XrGraphicsRequirementsD3D12KHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrGraphicsRequirementsD3D12KHR> = "XrGraphicsRequirementsD3D12KHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrGraphicsRequirementsD3D12KHR> = XR_TYPE_GRAPHICS_REQUIREMENTS_D3D12_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_REQUIREMENTS_D3D12_KHR> = "XrGraphicsRequirementsD3D12KHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrGraphicsRequirementsD3D12KHR> = true;
#endif // defined(XR_USE_GRAPHICS_API_D3D12)

        // ---- XR_KHR_visibility_mask extension

        template <> constexpr bool is_xr_tagged_type_impl_v<XrVisibilityMaskKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrVisibilityMaskKHR> = "XrVisibilityMaskKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrVisibilityMaskKHR> = XR_TYPE_VISIBILITY_MASK_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_VISIBILITY_MASK_KHR> = "XrVisibilityMaskKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrVisibilityMaskKHR> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrEventDataVisibilityMaskChangedKHR> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrEventDataVisibilityMaskChangedKHR> = "XrEventDataVisibilityMaskChangedKHR";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrEventDataVisibilityMaskChangedKHR> = XR_TYPE_EVENT_DATA_VISIBILITY_MASK_CHANGED_KHR;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_VISIBILITY_MASK_CHANGED_KHR> = "XrEventDataVisibilityMaskChangedKHR";
        template <> constexpr bool has_xr_type_tag_impl_v<XrEventDataVisibilityMaskChangedKHR> = true;

        // ---- XR_EXT_performance_settings extension

        template <> constexpr bool is_xr_tagged_type_impl_v<XrEventDataPerfSettingsEXT> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrEventDataPerfSettingsEXT> = "XrEventDataPerfSettingsEXT";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrEventDataPerfSettingsEXT> = XR_TYPE_EVENT_DATA_PERF_SETTINGS_EXT;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_PERF_SETTINGS_EXT> = "XrEventDataPerfSettingsEXT";
        template <> constexpr bool has_xr_type_tag_impl_v<XrEventDataPerfSettingsEXT> = true;

        // ---- XR_EXT_debug_utils extension

        template <> constexpr bool is_xr_tagged_type_impl_v<XrDebugUtilsObjectNameInfoEXT> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrDebugUtilsObjectNameInfoEXT> = "XrDebugUtilsObjectNameInfoEXT";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrDebugUtilsObjectNameInfoEXT> = XR_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT> = "XrDebugUtilsObjectNameInfoEXT";
        template <> constexpr bool has_xr_type_tag_impl_v<XrDebugUtilsObjectNameInfoEXT> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrDebugUtilsLabelEXT> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrDebugUtilsLabelEXT> = "XrDebugUtilsLabelEXT";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrDebugUtilsLabelEXT> = XR_TYPE_DEBUG_UTILS_LABEL_EXT;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_DEBUG_UTILS_LABEL_EXT> = "XrDebugUtilsLabelEXT";
        template <> constexpr bool has_xr_type_tag_impl_v<XrDebugUtilsLabelEXT> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrDebugUtilsMessengerCallbackDataEXT> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrDebugUtilsMessengerCallbackDataEXT> = "XrDebugUtilsMessengerCallbackDataEXT";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrDebugUtilsMessengerCallbackDataEXT> = XR_TYPE_DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT> = "XrDebugUtilsMessengerCallbackDataEXT";
        template <> constexpr bool has_xr_type_tag_impl_v<XrDebugUtilsMessengerCallbackDataEXT> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrDebugUtilsMessengerCreateInfoEXT> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrDebugUtilsMessengerCreateInfoEXT> = "XrDebugUtilsMessengerCreateInfoEXT";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrDebugUtilsMessengerCreateInfoEXT> = XR_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT> = "XrDebugUtilsMessengerCreateInfoEXT";
        template <> constexpr bool has_xr_type_tag_impl_v<XrDebugUtilsMessengerCreateInfoEXT> = true;

        // ---- XR_MSFT_spatial_anchor extension

        template <> constexpr bool is_xr_tagged_type_impl_v<XrSpatialAnchorCreateInfoMSFT> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSpatialAnchorCreateInfoMSFT> = "XrSpatialAnchorCreateInfoMSFT";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSpatialAnchorCreateInfoMSFT> = XR_TYPE_SPATIAL_ANCHOR_CREATE_INFO_MSFT;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SPATIAL_ANCHOR_CREATE_INFO_MSFT> = "XrSpatialAnchorCreateInfoMSFT";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSpatialAnchorCreateInfoMSFT> = true;

        template <> constexpr bool is_xr_tagged_type_impl_v<XrSpatialAnchorSpaceCreateInfoMSFT> = true;
        template <> constexpr const char * xr_type_name_impl_v<XrSpatialAnchorSpaceCreateInfoMSFT> = "XrSpatialAnchorSpaceCreateInfoMSFT";
        template <> constexpr XrStructureType xr_type_tag_impl_v<XrSpatialAnchorSpaceCreateInfoMSFT> = XR_TYPE_SPATIAL_ANCHOR_SPACE_CREATE_INFO_MSFT;
        template <> constexpr const char * xr_type_name_by_tag_impl_v<XR_TYPE_SPATIAL_ANCHOR_SPACE_CREATE_INFO_MSFT> = "XrSpatialAnchorSpaceCreateInfoMSFT";
        template <> constexpr bool has_xr_type_tag_impl_v<XrSpatialAnchorSpaceCreateInfoMSFT> = true;
        constexpr const char * structureTypeEnumToString(XrStructureType tag) {
            switch (tag) {

// ---- Core 1_0
                case XR_TYPE_API_LAYER_PROPERTIES:
                    return "XR_TYPE_API_LAYER_PROPERTIES";
                case XR_TYPE_EXTENSION_PROPERTIES:
                    return "XR_TYPE_EXTENSION_PROPERTIES";
                case XR_TYPE_INSTANCE_CREATE_INFO:
                    return "XR_TYPE_INSTANCE_CREATE_INFO";
                case XR_TYPE_INSTANCE_PROPERTIES:
                    return "XR_TYPE_INSTANCE_PROPERTIES";
                case XR_TYPE_EVENT_DATA_BUFFER:
                    return "XR_TYPE_EVENT_DATA_BUFFER";
                case XR_TYPE_SYSTEM_GET_INFO:
                    return "XR_TYPE_SYSTEM_GET_INFO";
                case XR_TYPE_SYSTEM_PROPERTIES:
                    return "XR_TYPE_SYSTEM_PROPERTIES";
                case XR_TYPE_SESSION_CREATE_INFO:
                    return "XR_TYPE_SESSION_CREATE_INFO";
                case XR_TYPE_SPACE_VELOCITY:
                    return "XR_TYPE_SPACE_VELOCITY";
                case XR_TYPE_REFERENCE_SPACE_CREATE_INFO:
                    return "XR_TYPE_REFERENCE_SPACE_CREATE_INFO";
                case XR_TYPE_ACTION_SPACE_CREATE_INFO:
                    return "XR_TYPE_ACTION_SPACE_CREATE_INFO";
                case XR_TYPE_SPACE_LOCATION:
                    return "XR_TYPE_SPACE_LOCATION";
                case XR_TYPE_VIEW_CONFIGURATION_PROPERTIES:
                    return "XR_TYPE_VIEW_CONFIGURATION_PROPERTIES";
                case XR_TYPE_VIEW_CONFIGURATION_VIEW:
                    return "XR_TYPE_VIEW_CONFIGURATION_VIEW";
                case XR_TYPE_SWAPCHAIN_CREATE_INFO:
                    return "XR_TYPE_SWAPCHAIN_CREATE_INFO";
                case XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO:
                    return "XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO";
                case XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO:
                    return "XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO";
                case XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO:
                    return "XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO";
                case XR_TYPE_SESSION_BEGIN_INFO:
                    return "XR_TYPE_SESSION_BEGIN_INFO";
                case XR_TYPE_FRAME_WAIT_INFO:
                    return "XR_TYPE_FRAME_WAIT_INFO";
                case XR_TYPE_FRAME_STATE:
                    return "XR_TYPE_FRAME_STATE";
                case XR_TYPE_FRAME_BEGIN_INFO:
                    return "XR_TYPE_FRAME_BEGIN_INFO";
                case XR_TYPE_FRAME_END_INFO:
                    return "XR_TYPE_FRAME_END_INFO";
                case XR_TYPE_VIEW_LOCATE_INFO:
                    return "XR_TYPE_VIEW_LOCATE_INFO";
                case XR_TYPE_VIEW_STATE:
                    return "XR_TYPE_VIEW_STATE";
                case XR_TYPE_VIEW:
                    return "XR_TYPE_VIEW";
                case XR_TYPE_ACTION_SET_CREATE_INFO:
                    return "XR_TYPE_ACTION_SET_CREATE_INFO";
                case XR_TYPE_ACTION_CREATE_INFO:
                    return "XR_TYPE_ACTION_CREATE_INFO";
                case XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING:
                    return "XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING";
                case XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO:
                    return "XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO";
                case XR_TYPE_INTERACTION_PROFILE_STATE:
                    return "XR_TYPE_INTERACTION_PROFILE_STATE";
                case XR_TYPE_ACTION_STATE_GET_INFO:
                    return "XR_TYPE_ACTION_STATE_GET_INFO";
                case XR_TYPE_ACTION_STATE_BOOLEAN:
                    return "XR_TYPE_ACTION_STATE_BOOLEAN";
                case XR_TYPE_ACTION_STATE_FLOAT:
                    return "XR_TYPE_ACTION_STATE_FLOAT";
                case XR_TYPE_ACTION_STATE_VECTOR2F:
                    return "XR_TYPE_ACTION_STATE_VECTOR2F";
                case XR_TYPE_ACTION_STATE_POSE:
                    return "XR_TYPE_ACTION_STATE_POSE";
                case XR_TYPE_ACTIONS_SYNC_INFO:
                    return "XR_TYPE_ACTIONS_SYNC_INFO";
                case XR_TYPE_BOUND_SOURCES_FOR_ACTION_ENUMERATE_INFO:
                    return "XR_TYPE_BOUND_SOURCES_FOR_ACTION_ENUMERATE_INFO";
                case XR_TYPE_INPUT_SOURCE_LOCALIZED_NAME_GET_INFO:
                    return "XR_TYPE_INPUT_SOURCE_LOCALIZED_NAME_GET_INFO";
                case XR_TYPE_HAPTIC_ACTION_INFO:
                    return "XR_TYPE_HAPTIC_ACTION_INFO";
                case XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW:
                    return "XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW";
                case XR_TYPE_COMPOSITION_LAYER_PROJECTION:
                    return "XR_TYPE_COMPOSITION_LAYER_PROJECTION";
                case XR_TYPE_COMPOSITION_LAYER_QUAD:
                    return "XR_TYPE_COMPOSITION_LAYER_QUAD";
                case XR_TYPE_EVENT_DATA_EVENTS_LOST:
                    return "XR_TYPE_EVENT_DATA_EVENTS_LOST";
                case XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING:
                    return "XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING";
                case XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED:
                    return "XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED";
                case XR_TYPE_EVENT_DATA_REFERENCE_SPACE_CHANGE_PENDING:
                    return "XR_TYPE_EVENT_DATA_REFERENCE_SPACE_CHANGE_PENDING";
                case XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED:
                    return "XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED";
                case XR_TYPE_HAPTIC_VIBRATION:
                    return "XR_TYPE_HAPTIC_VIBRATION";

// ---- XR_KHR_composition_layer_cube extension
                case XR_TYPE_COMPOSITION_LAYER_CUBE_KHR:
                    return "XR_TYPE_COMPOSITION_LAYER_CUBE_KHR";

// ---- XR_KHR_android_create_instance extension

#if defined(XR_USE_PLATFORM_ANDROID)
                case XR_TYPE_INSTANCE_CREATE_INFO_ANDROID_KHR:
                    return "XR_TYPE_INSTANCE_CREATE_INFO_ANDROID_KHR";
#endif // defined(XR_USE_PLATFORM_ANDROID)

// ---- XR_KHR_composition_layer_depth extension
                case XR_TYPE_COMPOSITION_LAYER_DEPTH_INFO_KHR:
                    return "XR_TYPE_COMPOSITION_LAYER_DEPTH_INFO_KHR";

// ---- XR_KHR_vulkan_swapchain_format_list extension

#if defined(XR_USE_GRAPHICS_API_VULKAN)
                case XR_TYPE_VULKAN_SWAPCHAIN_FORMAT_LIST_CREATE_INFO_KHR:
                    return "XR_TYPE_VULKAN_SWAPCHAIN_FORMAT_LIST_CREATE_INFO_KHR";
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

// ---- XR_KHR_composition_layer_cylinder extension
                case XR_TYPE_COMPOSITION_LAYER_CYLINDER_KHR:
                    return "XR_TYPE_COMPOSITION_LAYER_CYLINDER_KHR";

// ---- XR_KHR_composition_layer_equirect extension
                case XR_TYPE_COMPOSITION_LAYER_EQUIRECT_KHR:
                    return "XR_TYPE_COMPOSITION_LAYER_EQUIRECT_KHR";

// ---- XR_KHR_opengl_enable extension

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WIN32)
                case XR_TYPE_GRAPHICS_BINDING_OPENGL_WIN32_KHR:
                    return "XR_TYPE_GRAPHICS_BINDING_OPENGL_WIN32_KHR";
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WIN32)

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XLIB)
                case XR_TYPE_GRAPHICS_BINDING_OPENGL_XLIB_KHR:
                    return "XR_TYPE_GRAPHICS_BINDING_OPENGL_XLIB_KHR";
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XLIB)

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XCB)
                case XR_TYPE_GRAPHICS_BINDING_OPENGL_XCB_KHR:
                    return "XR_TYPE_GRAPHICS_BINDING_OPENGL_XCB_KHR";
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XCB)

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WAYLAND)
                case XR_TYPE_GRAPHICS_BINDING_OPENGL_WAYLAND_KHR:
                    return "XR_TYPE_GRAPHICS_BINDING_OPENGL_WAYLAND_KHR";
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WAYLAND)

#if defined(XR_USE_GRAPHICS_API_OPENGL)
                case XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_KHR:
                    return "XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_KHR";
#endif // defined(XR_USE_GRAPHICS_API_OPENGL)

#if defined(XR_USE_GRAPHICS_API_OPENGL)
                case XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_KHR:
                    return "XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_KHR";
#endif // defined(XR_USE_GRAPHICS_API_OPENGL)

// ---- XR_KHR_opengl_es_enable extension

#if defined(XR_USE_GRAPHICS_API_OPENGL_ES) && defined(XR_USE_PLATFORM_ANDROID)
                case XR_TYPE_GRAPHICS_BINDING_OPENGL_ES_ANDROID_KHR:
                    return "XR_TYPE_GRAPHICS_BINDING_OPENGL_ES_ANDROID_KHR";
#endif // defined(XR_USE_GRAPHICS_API_OPENGL_ES) && defined(XR_USE_PLATFORM_ANDROID)

#if defined(XR_USE_GRAPHICS_API_OPENGL_ES)
                case XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_ES_KHR:
                    return "XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_ES_KHR";
#endif // defined(XR_USE_GRAPHICS_API_OPENGL_ES)

#if defined(XR_USE_GRAPHICS_API_OPENGL_ES)
                case XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_ES_KHR:
                    return "XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_ES_KHR";
#endif // defined(XR_USE_GRAPHICS_API_OPENGL_ES)

// ---- XR_KHR_vulkan_enable extension

#if defined(XR_USE_GRAPHICS_API_VULKAN)
                case XR_TYPE_GRAPHICS_BINDING_VULKAN_KHR:
                    return "XR_TYPE_GRAPHICS_BINDING_VULKAN_KHR";
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

#if defined(XR_USE_GRAPHICS_API_VULKAN)
                case XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR:
                    return "XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR";
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

#if defined(XR_USE_GRAPHICS_API_VULKAN)
                case XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR:
                    return "XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR";
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

// ---- XR_KHR_D3D11_enable extension

#if defined(XR_USE_GRAPHICS_API_D3D11)
                case XR_TYPE_GRAPHICS_BINDING_D3D11_KHR:
                    return "XR_TYPE_GRAPHICS_BINDING_D3D11_KHR";
#endif // defined(XR_USE_GRAPHICS_API_D3D11)

#if defined(XR_USE_GRAPHICS_API_D3D11)
                case XR_TYPE_SWAPCHAIN_IMAGE_D3D11_KHR:
                    return "XR_TYPE_SWAPCHAIN_IMAGE_D3D11_KHR";
#endif // defined(XR_USE_GRAPHICS_API_D3D11)

#if defined(XR_USE_GRAPHICS_API_D3D11)
                case XR_TYPE_GRAPHICS_REQUIREMENTS_D3D11_KHR:
                    return "XR_TYPE_GRAPHICS_REQUIREMENTS_D3D11_KHR";
#endif // defined(XR_USE_GRAPHICS_API_D3D11)

// ---- XR_KHR_D3D12_enable extension

#if defined(XR_USE_GRAPHICS_API_D3D12)
                case XR_TYPE_GRAPHICS_BINDING_D3D12_KHR:
                    return "XR_TYPE_GRAPHICS_BINDING_D3D12_KHR";
#endif // defined(XR_USE_GRAPHICS_API_D3D12)

#if defined(XR_USE_GRAPHICS_API_D3D12)
                case XR_TYPE_SWAPCHAIN_IMAGE_D3D12_KHR:
                    return "XR_TYPE_SWAPCHAIN_IMAGE_D3D12_KHR";
#endif // defined(XR_USE_GRAPHICS_API_D3D12)

#if defined(XR_USE_GRAPHICS_API_D3D12)
                case XR_TYPE_GRAPHICS_REQUIREMENTS_D3D12_KHR:
                    return "XR_TYPE_GRAPHICS_REQUIREMENTS_D3D12_KHR";
#endif // defined(XR_USE_GRAPHICS_API_D3D12)

// ---- XR_KHR_visibility_mask extension
                case XR_TYPE_VISIBILITY_MASK_KHR:
                    return "XR_TYPE_VISIBILITY_MASK_KHR";
                case XR_TYPE_EVENT_DATA_VISIBILITY_MASK_CHANGED_KHR:
                    return "XR_TYPE_EVENT_DATA_VISIBILITY_MASK_CHANGED_KHR";

// ---- XR_EXT_performance_settings extension
                case XR_TYPE_EVENT_DATA_PERF_SETTINGS_EXT:
                    return "XR_TYPE_EVENT_DATA_PERF_SETTINGS_EXT";

// ---- XR_EXT_debug_utils extension
                case XR_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT:
                    return "XR_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT";
                case XR_TYPE_DEBUG_UTILS_LABEL_EXT:
                    return "XR_TYPE_DEBUG_UTILS_LABEL_EXT";
                case XR_TYPE_DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT:
                    return "XR_TYPE_DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT";
                case XR_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT:
                    return "XR_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT";

// ---- XR_MSFT_spatial_anchor extension
                case XR_TYPE_SPATIAL_ANCHOR_CREATE_INFO_MSFT:
                    return "XR_TYPE_SPATIAL_ANCHOR_CREATE_INFO_MSFT";
                case XR_TYPE_SPATIAL_ANCHOR_SPACE_CREATE_INFO_MSFT:
                    return "XR_TYPE_SPATIAL_ANCHOR_SPACE_CREATE_INFO_MSFT";
                default: return nullptr;
            }
        }

        constexpr const char * structureTypeEnumToTypeNameString(XrStructureType tag) {
            switch (tag) {

// ---- Core 1_0
                case XR_TYPE_API_LAYER_PROPERTIES:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_API_LAYER_PROPERTIES>;
                case XR_TYPE_EXTENSION_PROPERTIES:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_EXTENSION_PROPERTIES>;
                case XR_TYPE_INSTANCE_CREATE_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_INSTANCE_CREATE_INFO>;
                case XR_TYPE_INSTANCE_PROPERTIES:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_INSTANCE_PROPERTIES>;
                case XR_TYPE_EVENT_DATA_BUFFER:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_BUFFER>;
                case XR_TYPE_SYSTEM_GET_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SYSTEM_GET_INFO>;
                case XR_TYPE_SYSTEM_PROPERTIES:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SYSTEM_PROPERTIES>;
                case XR_TYPE_SESSION_CREATE_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SESSION_CREATE_INFO>;
                case XR_TYPE_SPACE_VELOCITY:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SPACE_VELOCITY>;
                case XR_TYPE_REFERENCE_SPACE_CREATE_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_REFERENCE_SPACE_CREATE_INFO>;
                case XR_TYPE_ACTION_SPACE_CREATE_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_SPACE_CREATE_INFO>;
                case XR_TYPE_SPACE_LOCATION:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SPACE_LOCATION>;
                case XR_TYPE_VIEW_CONFIGURATION_PROPERTIES:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_VIEW_CONFIGURATION_PROPERTIES>;
                case XR_TYPE_VIEW_CONFIGURATION_VIEW:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_VIEW_CONFIGURATION_VIEW>;
                case XR_TYPE_SWAPCHAIN_CREATE_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_CREATE_INFO>;
                case XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO>;
                case XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO>;
                case XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO>;
                case XR_TYPE_SESSION_BEGIN_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SESSION_BEGIN_INFO>;
                case XR_TYPE_FRAME_WAIT_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_FRAME_WAIT_INFO>;
                case XR_TYPE_FRAME_STATE:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_FRAME_STATE>;
                case XR_TYPE_FRAME_BEGIN_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_FRAME_BEGIN_INFO>;
                case XR_TYPE_FRAME_END_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_FRAME_END_INFO>;
                case XR_TYPE_VIEW_LOCATE_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_VIEW_LOCATE_INFO>;
                case XR_TYPE_VIEW_STATE:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_VIEW_STATE>;
                case XR_TYPE_VIEW:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_VIEW>;
                case XR_TYPE_ACTION_SET_CREATE_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_SET_CREATE_INFO>;
                case XR_TYPE_ACTION_CREATE_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_CREATE_INFO>;
                case XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING>;
                case XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO>;
                case XR_TYPE_INTERACTION_PROFILE_STATE:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_INTERACTION_PROFILE_STATE>;
                case XR_TYPE_ACTION_STATE_GET_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_STATE_GET_INFO>;
                case XR_TYPE_ACTION_STATE_BOOLEAN:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_STATE_BOOLEAN>;
                case XR_TYPE_ACTION_STATE_FLOAT:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_STATE_FLOAT>;
                case XR_TYPE_ACTION_STATE_VECTOR2F:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_STATE_VECTOR2F>;
                case XR_TYPE_ACTION_STATE_POSE:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_ACTION_STATE_POSE>;
                case XR_TYPE_ACTIONS_SYNC_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_ACTIONS_SYNC_INFO>;
                case XR_TYPE_BOUND_SOURCES_FOR_ACTION_ENUMERATE_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_BOUND_SOURCES_FOR_ACTION_ENUMERATE_INFO>;
                case XR_TYPE_INPUT_SOURCE_LOCALIZED_NAME_GET_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_INPUT_SOURCE_LOCALIZED_NAME_GET_INFO>;
                case XR_TYPE_HAPTIC_ACTION_INFO:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_HAPTIC_ACTION_INFO>;
                case XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW>;
                case XR_TYPE_COMPOSITION_LAYER_PROJECTION:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_COMPOSITION_LAYER_PROJECTION>;
                case XR_TYPE_COMPOSITION_LAYER_QUAD:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_COMPOSITION_LAYER_QUAD>;
                case XR_TYPE_EVENT_DATA_EVENTS_LOST:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_EVENTS_LOST>;
                case XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING>;
                case XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED>;
                case XR_TYPE_EVENT_DATA_REFERENCE_SPACE_CHANGE_PENDING:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_REFERENCE_SPACE_CHANGE_PENDING>;
                case XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED>;
                case XR_TYPE_HAPTIC_VIBRATION:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_HAPTIC_VIBRATION>;

// ---- XR_KHR_composition_layer_cube extension
                case XR_TYPE_COMPOSITION_LAYER_CUBE_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_COMPOSITION_LAYER_CUBE_KHR>;

// ---- XR_KHR_android_create_instance extension

#if defined(XR_USE_PLATFORM_ANDROID)
                case XR_TYPE_INSTANCE_CREATE_INFO_ANDROID_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_INSTANCE_CREATE_INFO_ANDROID_KHR>;
#endif // defined(XR_USE_PLATFORM_ANDROID)

// ---- XR_KHR_composition_layer_depth extension
                case XR_TYPE_COMPOSITION_LAYER_DEPTH_INFO_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_COMPOSITION_LAYER_DEPTH_INFO_KHR>;

// ---- XR_KHR_vulkan_swapchain_format_list extension

#if defined(XR_USE_GRAPHICS_API_VULKAN)
                case XR_TYPE_VULKAN_SWAPCHAIN_FORMAT_LIST_CREATE_INFO_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_VULKAN_SWAPCHAIN_FORMAT_LIST_CREATE_INFO_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

// ---- XR_KHR_composition_layer_cylinder extension
                case XR_TYPE_COMPOSITION_LAYER_CYLINDER_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_COMPOSITION_LAYER_CYLINDER_KHR>;

// ---- XR_KHR_composition_layer_equirect extension
                case XR_TYPE_COMPOSITION_LAYER_EQUIRECT_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_COMPOSITION_LAYER_EQUIRECT_KHR>;

// ---- XR_KHR_opengl_enable extension

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WIN32)
                case XR_TYPE_GRAPHICS_BINDING_OPENGL_WIN32_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_OPENGL_WIN32_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WIN32)

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XLIB)
                case XR_TYPE_GRAPHICS_BINDING_OPENGL_XLIB_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_OPENGL_XLIB_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XLIB)

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XCB)
                case XR_TYPE_GRAPHICS_BINDING_OPENGL_XCB_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_OPENGL_XCB_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XCB)

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WAYLAND)
                case XR_TYPE_GRAPHICS_BINDING_OPENGL_WAYLAND_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_OPENGL_WAYLAND_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WAYLAND)

#if defined(XR_USE_GRAPHICS_API_OPENGL)
                case XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL)

#if defined(XR_USE_GRAPHICS_API_OPENGL)
                case XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL)

// ---- XR_KHR_opengl_es_enable extension

#if defined(XR_USE_GRAPHICS_API_OPENGL_ES) && defined(XR_USE_PLATFORM_ANDROID)
                case XR_TYPE_GRAPHICS_BINDING_OPENGL_ES_ANDROID_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_OPENGL_ES_ANDROID_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL_ES) && defined(XR_USE_PLATFORM_ANDROID)

#if defined(XR_USE_GRAPHICS_API_OPENGL_ES)
                case XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_ES_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_ES_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL_ES)

#if defined(XR_USE_GRAPHICS_API_OPENGL_ES)
                case XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_ES_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_ES_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL_ES)

// ---- XR_KHR_vulkan_enable extension

#if defined(XR_USE_GRAPHICS_API_VULKAN)
                case XR_TYPE_GRAPHICS_BINDING_VULKAN_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_VULKAN_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

#if defined(XR_USE_GRAPHICS_API_VULKAN)
                case XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

#if defined(XR_USE_GRAPHICS_API_VULKAN)
                case XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

// ---- XR_KHR_D3D11_enable extension

#if defined(XR_USE_GRAPHICS_API_D3D11)
                case XR_TYPE_GRAPHICS_BINDING_D3D11_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_D3D11_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_D3D11)

#if defined(XR_USE_GRAPHICS_API_D3D11)
                case XR_TYPE_SWAPCHAIN_IMAGE_D3D11_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_D3D11_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_D3D11)

#if defined(XR_USE_GRAPHICS_API_D3D11)
                case XR_TYPE_GRAPHICS_REQUIREMENTS_D3D11_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_REQUIREMENTS_D3D11_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_D3D11)

// ---- XR_KHR_D3D12_enable extension

#if defined(XR_USE_GRAPHICS_API_D3D12)
                case XR_TYPE_GRAPHICS_BINDING_D3D12_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_BINDING_D3D12_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_D3D12)

#if defined(XR_USE_GRAPHICS_API_D3D12)
                case XR_TYPE_SWAPCHAIN_IMAGE_D3D12_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SWAPCHAIN_IMAGE_D3D12_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_D3D12)

#if defined(XR_USE_GRAPHICS_API_D3D12)
                case XR_TYPE_GRAPHICS_REQUIREMENTS_D3D12_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_GRAPHICS_REQUIREMENTS_D3D12_KHR>;
#endif // defined(XR_USE_GRAPHICS_API_D3D12)

// ---- XR_KHR_visibility_mask extension
                case XR_TYPE_VISIBILITY_MASK_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_VISIBILITY_MASK_KHR>;
                case XR_TYPE_EVENT_DATA_VISIBILITY_MASK_CHANGED_KHR:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_VISIBILITY_MASK_CHANGED_KHR>;

// ---- XR_EXT_performance_settings extension
                case XR_TYPE_EVENT_DATA_PERF_SETTINGS_EXT:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_EVENT_DATA_PERF_SETTINGS_EXT>;

// ---- XR_EXT_debug_utils extension
                case XR_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT>;
                case XR_TYPE_DEBUG_UTILS_LABEL_EXT:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_DEBUG_UTILS_LABEL_EXT>;
                case XR_TYPE_DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT>;
                case XR_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT>;

// ---- XR_MSFT_spatial_anchor extension
                case XR_TYPE_SPATIAL_ANCHOR_CREATE_INFO_MSFT:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SPATIAL_ANCHOR_CREATE_INFO_MSFT>;
                case XR_TYPE_SPATIAL_ANCHOR_SPACE_CREATE_INFO_MSFT:
                    return xr_type_name_by_tag_impl_v<XR_TYPE_SPATIAL_ANCHOR_SPACE_CREATE_INFO_MSFT>;
                default: return nullptr;
            }
        }

    } // namespace detail

template<typename F>
constexpr void typeDispatch(XrStructureType t, F&& f) {
        switch (t) {

// ---- Core 1_0
            case XR_TYPE_API_LAYER_PROPERTIES:
                f(TypeWrapper<XrApiLayerProperties>{});
                break;
            case XR_TYPE_EXTENSION_PROPERTIES:
                f(TypeWrapper<XrExtensionProperties>{});
                break;
            case XR_TYPE_INSTANCE_CREATE_INFO:
                f(TypeWrapper<XrInstanceCreateInfo>{});
                break;
            case XR_TYPE_INSTANCE_PROPERTIES:
                f(TypeWrapper<XrInstanceProperties>{});
                break;
            case XR_TYPE_EVENT_DATA_BUFFER:
                f(TypeWrapper<XrEventDataBuffer>{});
                break;
            case XR_TYPE_SYSTEM_GET_INFO:
                f(TypeWrapper<XrSystemGetInfo>{});
                break;
            case XR_TYPE_SYSTEM_PROPERTIES:
                f(TypeWrapper<XrSystemProperties>{});
                break;
            case XR_TYPE_SESSION_CREATE_INFO:
                f(TypeWrapper<XrSessionCreateInfo>{});
                break;
            case XR_TYPE_SPACE_VELOCITY:
                f(TypeWrapper<XrSpaceVelocity>{});
                break;
            case XR_TYPE_REFERENCE_SPACE_CREATE_INFO:
                f(TypeWrapper<XrReferenceSpaceCreateInfo>{});
                break;
            case XR_TYPE_ACTION_SPACE_CREATE_INFO:
                f(TypeWrapper<XrActionSpaceCreateInfo>{});
                break;
            case XR_TYPE_SPACE_LOCATION:
                f(TypeWrapper<XrSpaceLocation>{});
                break;
            case XR_TYPE_VIEW_CONFIGURATION_PROPERTIES:
                f(TypeWrapper<XrViewConfigurationProperties>{});
                break;
            case XR_TYPE_VIEW_CONFIGURATION_VIEW:
                f(TypeWrapper<XrViewConfigurationView>{});
                break;
            case XR_TYPE_SWAPCHAIN_CREATE_INFO:
                f(TypeWrapper<XrSwapchainCreateInfo>{});
                break;
            case XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO:
                f(TypeWrapper<XrSwapchainImageAcquireInfo>{});
                break;
            case XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO:
                f(TypeWrapper<XrSwapchainImageWaitInfo>{});
                break;
            case XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO:
                f(TypeWrapper<XrSwapchainImageReleaseInfo>{});
                break;
            case XR_TYPE_SESSION_BEGIN_INFO:
                f(TypeWrapper<XrSessionBeginInfo>{});
                break;
            case XR_TYPE_FRAME_WAIT_INFO:
                f(TypeWrapper<XrFrameWaitInfo>{});
                break;
            case XR_TYPE_FRAME_STATE:
                f(TypeWrapper<XrFrameState>{});
                break;
            case XR_TYPE_FRAME_BEGIN_INFO:
                f(TypeWrapper<XrFrameBeginInfo>{});
                break;
            case XR_TYPE_FRAME_END_INFO:
                f(TypeWrapper<XrFrameEndInfo>{});
                break;
            case XR_TYPE_VIEW_LOCATE_INFO:
                f(TypeWrapper<XrViewLocateInfo>{});
                break;
            case XR_TYPE_VIEW_STATE:
                f(TypeWrapper<XrViewState>{});
                break;
            case XR_TYPE_VIEW:
                f(TypeWrapper<XrView>{});
                break;
            case XR_TYPE_ACTION_SET_CREATE_INFO:
                f(TypeWrapper<XrActionSetCreateInfo>{});
                break;
            case XR_TYPE_ACTION_CREATE_INFO:
                f(TypeWrapper<XrActionCreateInfo>{});
                break;
            case XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING:
                f(TypeWrapper<XrInteractionProfileSuggestedBinding>{});
                break;
            case XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO:
                f(TypeWrapper<XrSessionActionSetsAttachInfo>{});
                break;
            case XR_TYPE_INTERACTION_PROFILE_STATE:
                f(TypeWrapper<XrInteractionProfileState>{});
                break;
            case XR_TYPE_ACTION_STATE_GET_INFO:
                f(TypeWrapper<XrActionStateGetInfo>{});
                break;
            case XR_TYPE_ACTION_STATE_BOOLEAN:
                f(TypeWrapper<XrActionStateBoolean>{});
                break;
            case XR_TYPE_ACTION_STATE_FLOAT:
                f(TypeWrapper<XrActionStateFloat>{});
                break;
            case XR_TYPE_ACTION_STATE_VECTOR2F:
                f(TypeWrapper<XrActionStateVector2f>{});
                break;
            case XR_TYPE_ACTION_STATE_POSE:
                f(TypeWrapper<XrActionStatePose>{});
                break;
            case XR_TYPE_ACTIONS_SYNC_INFO:
                f(TypeWrapper<XrActionsSyncInfo>{});
                break;
            case XR_TYPE_BOUND_SOURCES_FOR_ACTION_ENUMERATE_INFO:
                f(TypeWrapper<XrBoundSourcesForActionEnumerateInfo>{});
                break;
            case XR_TYPE_INPUT_SOURCE_LOCALIZED_NAME_GET_INFO:
                f(TypeWrapper<XrInputSourceLocalizedNameGetInfo>{});
                break;
            case XR_TYPE_HAPTIC_ACTION_INFO:
                f(TypeWrapper<XrHapticActionInfo>{});
                break;
            case XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW:
                f(TypeWrapper<XrCompositionLayerProjectionView>{});
                break;
            case XR_TYPE_COMPOSITION_LAYER_PROJECTION:
                f(TypeWrapper<XrCompositionLayerProjection>{});
                break;
            case XR_TYPE_COMPOSITION_LAYER_QUAD:
                f(TypeWrapper<XrCompositionLayerQuad>{});
                break;
            case XR_TYPE_EVENT_DATA_EVENTS_LOST:
                f(TypeWrapper<XrEventDataEventsLost>{});
                break;
            case XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING:
                f(TypeWrapper<XrEventDataInstanceLossPending>{});
                break;
            case XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED:
                f(TypeWrapper<XrEventDataSessionStateChanged>{});
                break;
            case XR_TYPE_EVENT_DATA_REFERENCE_SPACE_CHANGE_PENDING:
                f(TypeWrapper<XrEventDataReferenceSpaceChangePending>{});
                break;
            case XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED:
                f(TypeWrapper<XrEventDataInteractionProfileChanged>{});
                break;
            case XR_TYPE_HAPTIC_VIBRATION:
                f(TypeWrapper<XrHapticVibration>{});
                break;

// ---- XR_KHR_composition_layer_cube extension
            case XR_TYPE_COMPOSITION_LAYER_CUBE_KHR:
                f(TypeWrapper<XrCompositionLayerCubeKHR>{});
                break;

// ---- XR_KHR_android_create_instance extension

#if defined(XR_USE_PLATFORM_ANDROID)
            case XR_TYPE_INSTANCE_CREATE_INFO_ANDROID_KHR:
                f(TypeWrapper<XrInstanceCreateInfoAndroidKHR>{});
                break;
#endif // defined(XR_USE_PLATFORM_ANDROID)

// ---- XR_KHR_composition_layer_depth extension
            case XR_TYPE_COMPOSITION_LAYER_DEPTH_INFO_KHR:
                f(TypeWrapper<XrCompositionLayerDepthInfoKHR>{});
                break;

// ---- XR_KHR_vulkan_swapchain_format_list extension

#if defined(XR_USE_GRAPHICS_API_VULKAN)
            case XR_TYPE_VULKAN_SWAPCHAIN_FORMAT_LIST_CREATE_INFO_KHR:
                f(TypeWrapper<XrVulkanSwapchainFormatListCreateInfoKHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

// ---- XR_KHR_composition_layer_cylinder extension
            case XR_TYPE_COMPOSITION_LAYER_CYLINDER_KHR:
                f(TypeWrapper<XrCompositionLayerCylinderKHR>{});
                break;

// ---- XR_KHR_composition_layer_equirect extension
            case XR_TYPE_COMPOSITION_LAYER_EQUIRECT_KHR:
                f(TypeWrapper<XrCompositionLayerEquirectKHR>{});
                break;

// ---- XR_KHR_opengl_enable extension

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WIN32)
            case XR_TYPE_GRAPHICS_BINDING_OPENGL_WIN32_KHR:
                f(TypeWrapper<XrGraphicsBindingOpenGLWin32KHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WIN32)

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XLIB)
            case XR_TYPE_GRAPHICS_BINDING_OPENGL_XLIB_KHR:
                f(TypeWrapper<XrGraphicsBindingOpenGLXlibKHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XLIB)

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XCB)
            case XR_TYPE_GRAPHICS_BINDING_OPENGL_XCB_KHR:
                f(TypeWrapper<XrGraphicsBindingOpenGLXcbKHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XCB)

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WAYLAND)
            case XR_TYPE_GRAPHICS_BINDING_OPENGL_WAYLAND_KHR:
                f(TypeWrapper<XrGraphicsBindingOpenGLWaylandKHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_WAYLAND)

#if defined(XR_USE_GRAPHICS_API_OPENGL)
            case XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_KHR:
                f(TypeWrapper<XrSwapchainImageOpenGLKHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL)

#if defined(XR_USE_GRAPHICS_API_OPENGL)
            case XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_KHR:
                f(TypeWrapper<XrGraphicsRequirementsOpenGLKHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL)

// ---- XR_KHR_opengl_es_enable extension

#if defined(XR_USE_GRAPHICS_API_OPENGL_ES) && defined(XR_USE_PLATFORM_ANDROID)
            case XR_TYPE_GRAPHICS_BINDING_OPENGL_ES_ANDROID_KHR:
                f(TypeWrapper<XrGraphicsBindingOpenGLESAndroidKHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL_ES) && defined(XR_USE_PLATFORM_ANDROID)

#if defined(XR_USE_GRAPHICS_API_OPENGL_ES)
            case XR_TYPE_SWAPCHAIN_IMAGE_OPENGL_ES_KHR:
                f(TypeWrapper<XrSwapchainImageOpenGLESKHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL_ES)

#if defined(XR_USE_GRAPHICS_API_OPENGL_ES)
            case XR_TYPE_GRAPHICS_REQUIREMENTS_OPENGL_ES_KHR:
                f(TypeWrapper<XrGraphicsRequirementsOpenGLESKHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_OPENGL_ES)

// ---- XR_KHR_vulkan_enable extension

#if defined(XR_USE_GRAPHICS_API_VULKAN)
            case XR_TYPE_GRAPHICS_BINDING_VULKAN_KHR:
                f(TypeWrapper<XrGraphicsBindingVulkanKHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

#if defined(XR_USE_GRAPHICS_API_VULKAN)
            case XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR:
                f(TypeWrapper<XrSwapchainImageVulkanKHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

#if defined(XR_USE_GRAPHICS_API_VULKAN)
            case XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR:
                f(TypeWrapper<XrGraphicsRequirementsVulkanKHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_VULKAN)

// ---- XR_KHR_D3D11_enable extension

#if defined(XR_USE_GRAPHICS_API_D3D11)
            case XR_TYPE_GRAPHICS_BINDING_D3D11_KHR:
                f(TypeWrapper<XrGraphicsBindingD3D11KHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_D3D11)

#if defined(XR_USE_GRAPHICS_API_D3D11)
            case XR_TYPE_SWAPCHAIN_IMAGE_D3D11_KHR:
                f(TypeWrapper<XrSwapchainImageD3D11KHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_D3D11)

#if defined(XR_USE_GRAPHICS_API_D3D11)
            case XR_TYPE_GRAPHICS_REQUIREMENTS_D3D11_KHR:
                f(TypeWrapper<XrGraphicsRequirementsD3D11KHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_D3D11)

// ---- XR_KHR_D3D12_enable extension

#if defined(XR_USE_GRAPHICS_API_D3D12)
            case XR_TYPE_GRAPHICS_BINDING_D3D12_KHR:
                f(TypeWrapper<XrGraphicsBindingD3D12KHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_D3D12)

#if defined(XR_USE_GRAPHICS_API_D3D12)
            case XR_TYPE_SWAPCHAIN_IMAGE_D3D12_KHR:
                f(TypeWrapper<XrSwapchainImageD3D12KHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_D3D12)

#if defined(XR_USE_GRAPHICS_API_D3D12)
            case XR_TYPE_GRAPHICS_REQUIREMENTS_D3D12_KHR:
                f(TypeWrapper<XrGraphicsRequirementsD3D12KHR>{});
                break;
#endif // defined(XR_USE_GRAPHICS_API_D3D12)

// ---- XR_KHR_visibility_mask extension
            case XR_TYPE_VISIBILITY_MASK_KHR:
                f(TypeWrapper<XrVisibilityMaskKHR>{});
                break;
            case XR_TYPE_EVENT_DATA_VISIBILITY_MASK_CHANGED_KHR:
                f(TypeWrapper<XrEventDataVisibilityMaskChangedKHR>{});
                break;

// ---- XR_EXT_performance_settings extension
            case XR_TYPE_EVENT_DATA_PERF_SETTINGS_EXT:
                f(TypeWrapper<XrEventDataPerfSettingsEXT>{});
                break;

// ---- XR_EXT_debug_utils extension
            case XR_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT:
                f(TypeWrapper<XrDebugUtilsObjectNameInfoEXT>{});
                break;
            case XR_TYPE_DEBUG_UTILS_LABEL_EXT:
                f(TypeWrapper<XrDebugUtilsLabelEXT>{});
                break;
            case XR_TYPE_DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT:
                f(TypeWrapper<XrDebugUtilsMessengerCallbackDataEXT>{});
                break;
            case XR_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT:
                f(TypeWrapper<XrDebugUtilsMessengerCreateInfoEXT>{});
                break;

// ---- XR_MSFT_spatial_anchor extension
            case XR_TYPE_SPATIAL_ANCHOR_CREATE_INFO_MSFT:
                f(TypeWrapper<XrSpatialAnchorCreateInfoMSFT>{});
                break;
            case XR_TYPE_SPATIAL_ANCHOR_SPACE_CREATE_INFO_MSFT:
                f(TypeWrapper<XrSpatialAnchorSpaceCreateInfoMSFT>{});
                break;
            default: break;
        }
}
} // namespace traits
} // namespace xrtraits


