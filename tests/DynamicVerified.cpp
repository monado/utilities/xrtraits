// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Test of DynamicVerified functionality.
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "xrtraits/DynamicVerified.h"
#include "xrtraits/InitXrType.h"

// Library Includes
#include <catch2/catch.hpp>

// Standard Includes
// - none

using namespace xrtraits;

using Catch::Contains;

#ifdef XRTRAITS_HAVE_DYNAMIC_VERIFIED

CATCH_TRANSLATE_EXCEPTION(exceptions::xr_exception& ex) { return ex.what(); }

TEST_CASE("verifyDynamicType")
{
	WHEN("Passing pointer to XrActionSetCreateInfo")
	{
		XrActionSetCreateInfo actionSetCreateInfo = {XR_TYPE_ACTION_SET_CREATE_INFO};
		THEN("Should return same non-nullptr input pointer without "
		     "throwing")
		{
			REQUIRE_NOTHROW(verifyDynamicType(&actionSetCreateInfo));

			CHECK_FALSE(verifyDynamicType(&actionSetCreateInfo) == nullptr);
			CHECK_FALSE(nullptr == verifyDynamicType(&actionSetCreateInfo));

			CHECK(verifyDynamicType(&actionSetCreateInfo).get() == &actionSetCreateInfo);
		}
	}

	WHEN("Passing reference to XrActionSetCreateInfo")
	{
		XrActionSetCreateInfo actionSetCreateInfo = {XR_TYPE_ACTION_SET_CREATE_INFO};
		THEN("Should return pointer to same input without "
		     "throwing")
		{
			REQUIRE_NOTHROW(verifyDynamicType(actionSetCreateInfo));

			CHECK_FALSE(verifyDynamicType(actionSetCreateInfo) == nullptr);
			CHECK_FALSE(nullptr == verifyDynamicType(actionSetCreateInfo));

			CHECK(verifyDynamicType(actionSetCreateInfo).get() == &actionSetCreateInfo);
		}
	}

	WHEN("Passing reference to XrActionSetCreateInfo initialized with "
	     "different but known type member")
	{
		XrActionSetCreateInfo wrongInfo = {XR_TYPE_INSTANCE_CREATE_INFO};
		THEN("Should throw a validation error")
		{
			REQUIRE_THROWS_WITH(verifyDynamicType(wrongInfo),
			                    Contains("a struct of type XrInstanceCreateInfo"));
			REQUIRE_THROWS_WITH(verifyDynamicType(wrongInfo), Contains("instead of the expected type "
			                                                           "XrActionSetCreateInfo"));
		}
	}

	WHEN("Passing reference to XrActionSetCreateInfo initialized with "
	     "an unknown type member")
	{
		XrActionSetCreateInfo wrongInfo = {XR_STRUCTURE_TYPE_MAX_ENUM};
		THEN("Should throw a validation error")
		{
			REQUIRE_THROWS_WITH(verifyDynamicType(wrongInfo), Contains("a struct of unknown type"));
			REQUIRE_THROWS_WITH(verifyDynamicType(wrongInfo), Contains("instead of the expected type "
			                                                           "XrActionSetCreateInfo"));
			REQUIRE_THROWS_WITH(verifyDynamicType(wrongInfo), Contains("Parameter is a struct"));
		}
		AND_WHEN("A parameter name is passed")
		{
			THEN("Message start should be right")
			{
				REQUIRE_THROWS_WITH(verifyDynamicType(wrongInfo, "p"), Contains("Parameter p is"));
			}
		}
	}

	WHEN("Passing null XrActionSetCreateInfo pointer")
	{
		XrActionSetCreateInfo* nullCreateInfo = nullptr;
		THEN("Should throw a validation error")
		{
			REQUIRE_THROWS_WITH(verifyDynamicType(nullCreateInfo), Contains("Parameter is nullptr"));
			REQUIRE_THROWS_WITH(verifyDynamicType(nullCreateInfo), Contains("instead of the expected type "
			                                                                "XrActionSetCreateInfo"));
		}
		AND_WHEN("A parameter name is passed")
		{
			THEN("Message start should be right")
			{
				REQUIRE_THROWS_WITH(verifyDynamicType(nullCreateInfo, "p"),
				                    Contains("Parameter p is nullptr"));
			}
		}
	}
}

TEST_CASE("DynamicVerified_behavior")
{
	WHEN("Calling verifyDynamicType with a reference to "
	     "XrActionSetCreateInfo")
	{
		XrActionSetCreateInfo info = {XR_TYPE_ACTION_SET_CREATE_INFO};
		THEN("Return value (temporary) should permit member access")
		{
			CHECK(verifyDynamicType(info)->type == XR_TYPE_ACTION_SET_CREATE_INFO);
			CHECK((*verifyDynamicType(info)).type == XR_TYPE_ACTION_SET_CREATE_INFO);
			CHECK(verifyDynamicType(info).get()->type == XR_TYPE_ACTION_SET_CREATE_INFO);
		}
		THEN("Return value (temporary) wrapped pointer should match")
		{
			CHECK(verifyDynamicType(info).get() == &info);
		}
		AND_WHEN("Return value is stored with auto verified =")
		{

			auto verified = verifyDynamicType(info);
			THEN("Should permit member access")
			{
				CHECK(verified->type == XR_TYPE_ACTION_SET_CREATE_INFO);
				CHECK((*verified).type == XR_TYPE_ACTION_SET_CREATE_INFO);
				CHECK(verified.get()->type == XR_TYPE_ACTION_SET_CREATE_INFO);
			}
			THEN("Wrapped pointer should match")
			{
				CHECK(verified.get() == verifyDynamicType(info).get());
				CHECK(verified.get() == &info);
			}
			AND_THEN("self-assignment should not break")
			{
				verified = verified;
				CHECK(verified->type == XR_TYPE_ACTION_SET_CREATE_INFO);
				CHECK((*verified).type == XR_TYPE_ACTION_SET_CREATE_INFO);
				CHECK(verified.get()->type == XR_TYPE_ACTION_SET_CREATE_INFO);

				CHECK(verified.get() == verifyDynamicType(info).get());
			}
			AND_THEN("assignment should re-seat")
			{
				XrActionSetCreateInfo info2 = {XR_TYPE_ACTION_SET_CREATE_INFO};
				verified = verifyDynamicType(info2);

				CHECK(verified.get() == verifyDynamicType(info2).get());

				CHECK(verified.get() == &info2);

				CHECK_FALSE(verified.get() == verifyDynamicType(info).get());
				CHECK_FALSE(verified.get() == &info);
			}
		}
	}
}

TEST_CASE("getFromChain")
{
	WHEN("Passing pointer to desired type")
	{
		XrActionSetCreateInfo ascInfo = {XR_TYPE_ACTION_SET_CREATE_INFO};
		THEN("Should return same non-nullptr input pointer without "
		     "throwing")
		{
			REQUIRE_NOTHROW(getFromChain<XrActionSetCreateInfo>(&ascInfo));

			CHECK_FALSE(getFromChain<XrActionSetCreateInfo>(&ascInfo) == nullptr);

			CHECK(getFromChain<XrActionSetCreateInfo>(&ascInfo).get() == &ascInfo);
		}
	}

	WHEN("Passing reference to desired type")
	{
		XrActionSetCreateInfo ascInfo = {XR_TYPE_ACTION_SET_CREATE_INFO};
		THEN("Should return pointer to same input without "
		     "throwing")
		{
			REQUIRE_NOTHROW(getFromChain<XrActionSetCreateInfo>(ascInfo));

			CHECK_FALSE(getFromChain<XrActionSetCreateInfo>(ascInfo) == nullptr);

			CHECK(getFromChain<XrActionSetCreateInfo>(ascInfo).get() == &ascInfo);
		}
	}

	WHEN("Passing reference to desired type initialized with "
	     "different but known type member")
	{
		XrActionSetCreateInfo wrongInfo = {XR_TYPE_INSTANCE_CREATE_INFO};
		THEN("Should throw a validation error")
		{
			REQUIRE_THROWS_WITH(getFromChain<XrActionSetCreateInfo>(wrongInfo),
			                    Contains("starting with type "
			                             "XrInstanceCreateInfo"));

			REQUIRE_THROWS_WITH(getFromChain<XrActionSetCreateInfo>(wrongInfo),
			                    Contains("that did not include the expected type "
			                             "XrActionSetCreateInfo"));
		}
	}

	WHEN("Passing reference to desired type initialized with "
	     "an unknown type member")
	{
		XrActionSetCreateInfo wrongInfo = {XR_STRUCTURE_TYPE_MAX_ENUM};
		THEN("Should throw a validation error")
		{
			REQUIRE_THROWS_WITH(getFromChain<XrActionSetCreateInfo>(wrongInfo),
			                    Contains("starting with unknown type"));

			REQUIRE_THROWS_WITH(getFromChain<XrActionSetCreateInfo>(wrongInfo),
			                    Contains("that did not include the expected type "
			                             "XrActionSetCreateInfo"));

			REQUIRE_THROWS_WITH(getFromChain<XrActionSetCreateInfo>(wrongInfo),
			                    Contains("Parameter is a chain"));
		}
		AND_WHEN("A parameter name is passed")
		{
			THEN("Message start should be right")
			{
				REQUIRE_THROWS_WITH(getFromChain<XrActionSetCreateInfo>(wrongInfo, "p"),
				                    Contains("Parameter p is a chain"));
			}
		}
	}

	WHEN("Passing null XrActionSetCreateInfo pointer")
	{
		XrActionSetCreateInfo* nullCreateInfo = nullptr;
		THEN("Should throw a validation error")
		{
			REQUIRE_THROWS_WITH(getFromChain<XrActionSetCreateInfo>(nullCreateInfo),
			                    Contains("Parameter is nullptr"));

			REQUIRE_THROWS_WITH(getFromChain<XrActionSetCreateInfo>(nullCreateInfo),
			                    Contains("containing the expected type "
			                             "XrActionSetCreateInfo"));
		}
		AND_WHEN("A parameter name is passed")
		{
			THEN("Message start should be right")
			{
				REQUIRE_THROWS_WITH(verifyDynamicType(nullCreateInfo, "p"),
				                    Contains("Parameter p is nullptr"));
			}
		}
	}
	WHEN("A struct chains to the desired type")
	{

		XrActionSetCreateInfo ascInfo = {XR_TYPE_ACTION_SET_CREATE_INFO};
		XrInstanceCreateInfo icInfo = {XR_TYPE_INSTANCE_CREATE_INFO, &ascInfo};
		AND_WHEN("getFromChain called with pointer")
		{
			THEN("Should return correct non-nullptr pointer "
			     "without throwing")
			{
				REQUIRE_NOTHROW(getFromChain<XrActionSetCreateInfo>(&icInfo));

				CHECK_FALSE(getFromChain<XrActionSetCreateInfo>(&icInfo) == nullptr);

				CHECK(getFromChain<XrActionSetCreateInfo>(&icInfo).get() == &ascInfo);
			}
		}

		AND_WHEN("getFromChain called with reference")
		{
			THEN("Should return correct non-nullptr pointer "
			     "without throwing")
			{
				REQUIRE_NOTHROW(getFromChain<XrActionSetCreateInfo>(icInfo));

				CHECK_FALSE(getFromChain<XrActionSetCreateInfo>(icInfo) == nullptr);

				CHECK(getFromChain<XrActionSetCreateInfo>(icInfo).get() == &ascInfo);
			}
		}
		AND_WHEN("getFromChain called with temporary DynamicVerified")
		{
			THEN("Should return correct non-nullptr pointer "
			     "without throwing")
			{
				REQUIRE_NOTHROW(getFromChain<XrActionSetCreateInfo>(verifyDynamicType(icInfo)));

				CHECK_FALSE(getFromChain<XrActionSetCreateInfo>(verifyDynamicType(icInfo)) == nullptr);

				CHECK(getFromChain<XrActionSetCreateInfo>(verifyDynamicType(icInfo)).get() == &ascInfo);
			}
		}

		AND_WHEN("getFromChain called with non-temporary DynamicVerified")
		{
			THEN("Should return correct non-nullptr pointer "
			     "without throwing")
			{
				auto icInfoVerified = verifyDynamicType(icInfo);
				REQUIRE_NOTHROW(getFromChain<XrActionSetCreateInfo>(icInfoVerified));

				CHECK_FALSE(getFromChain<XrActionSetCreateInfo>(icInfoVerified) == nullptr);

				CHECK(getFromChain<XrActionSetCreateInfo>(icInfoVerified).get() == &ascInfo);
			}
		}
	}
}

TEST_CASE("verifyDynamicTypeOrNull")
{
	WHEN("Passing pointer to XrActionSetCreateInfo")
	{
		XrActionSetCreateInfo actionSetCreateInfo = {XR_TYPE_ACTION_SET_CREATE_INFO};
		THEN("Should return same non-nullptr input pointer without "
		     "throwing")
		{
			REQUIRE_NOTHROW(verifyDynamicTypeOrNull(&actionSetCreateInfo));

			CHECK_FALSE(verifyDynamicTypeOrNull(&actionSetCreateInfo) == nullptr);
			CHECK_FALSE(nullptr == verifyDynamicTypeOrNull(&actionSetCreateInfo));

			CHECK(verifyDynamicTypeOrNull(&actionSetCreateInfo).get() == &actionSetCreateInfo);
		}
	}

	WHEN("Passing pointer to XrActionSetCreateInfo initialized with "
	     "different but known type member")
	{
		XrActionSetCreateInfo wrongInfo = {XR_TYPE_INSTANCE_CREATE_INFO};
		THEN("Should throw a validation error")
		{
			REQUIRE_THROWS_WITH(verifyDynamicTypeOrNull(&wrongInfo),
			                    Contains("a struct of type XrInstanceCreateInfo"));
			REQUIRE_THROWS_WITH(verifyDynamicTypeOrNull(&wrongInfo),
			                    Contains("instead of the expected type "
			                             "XrActionSetCreateInfo"));
		}
	}

	WHEN("Passing pointer to XrActionSetCreateInfo initialized with "
	     "an unknown type member")
	{
		XrActionSetCreateInfo wrongInfo = {XR_STRUCTURE_TYPE_MAX_ENUM};
		THEN("Should throw a validation error")
		{
			REQUIRE_THROWS_WITH(verifyDynamicTypeOrNull(&wrongInfo), Contains("a struct of unknown type"));
			REQUIRE_THROWS_WITH(verifyDynamicTypeOrNull(&wrongInfo),
			                    Contains("instead of the expected type "
			                             "XrActionSetCreateInfo"));
			REQUIRE_THROWS_WITH(verifyDynamicTypeOrNull(&wrongInfo), Contains("Parameter is a struct"));
		}
		AND_WHEN("A parameter name is passed")
		{
			THEN("Message start should be right")
			{
				REQUIRE_THROWS_WITH(verifyDynamicTypeOrNull(&wrongInfo, "p"),
				                    Contains("Parameter p is"));
			}
		}
	}

	WHEN("Passing null XrActionSetCreateInfo pointer")
	{
		XrActionSetCreateInfo* nullCreateInfo = nullptr;
		THEN("Doesn't throw") { REQUIRE_NOTHROW(verifyDynamicTypeOrNull(nullCreateInfo)); }
		THEN("Equal to nullptr") { CHECK(verifyDynamicTypeOrNull(nullCreateInfo) == nullptr); }
	}
}

TEST_CASE("DynamicVerifiedOrNull_behavior")
{
	WHEN("Calling verifyDynamicTypeOrNull with a pointer to "
	     "XrActionSetCreateInfo")
	{
		XrActionSetCreateInfo info = {XR_TYPE_ACTION_SET_CREATE_INFO};
		THEN("Return value (temporary) should permit member access")
		{
			CHECK(verifyDynamicTypeOrNull(&info)->type == XR_TYPE_ACTION_SET_CREATE_INFO);
			CHECK((*verifyDynamicTypeOrNull(&info)).type == XR_TYPE_ACTION_SET_CREATE_INFO);
			CHECK(verifyDynamicTypeOrNull(&info).get()->type == XR_TYPE_ACTION_SET_CREATE_INFO);
		}
		THEN("Return value (temporary) wrapped pointer should match")
		{
			CHECK(verifyDynamicTypeOrNull(&info).get() == &info);
		}
		AND_WHEN("Return value is stored with auto verified =")
		{

			auto verified = verifyDynamicTypeOrNull(&info);
			THEN("Should permit member access")
			{
				CHECK(verified->type == XR_TYPE_ACTION_SET_CREATE_INFO);
				CHECK((*verified).type == XR_TYPE_ACTION_SET_CREATE_INFO);
				CHECK(verified.get()->type == XR_TYPE_ACTION_SET_CREATE_INFO);
			}
			THEN("Wrapped pointer should match")
			{
				CHECK(verified.get() == verifyDynamicTypeOrNull(&info).get());
				CHECK(verified.get() == &info);
			}
			AND_THEN("self-assignment should not break")
			{
				verified = verified;
				CHECK(verified->type == XR_TYPE_ACTION_SET_CREATE_INFO);
				CHECK((*verified).type == XR_TYPE_ACTION_SET_CREATE_INFO);
				CHECK(verified.get()->type == XR_TYPE_ACTION_SET_CREATE_INFO);

				CHECK(verified.get() == verifyDynamicTypeOrNull(&info).get());
			}
			AND_THEN("assignment should re-seat")
			{
				XrActionSetCreateInfo info2 = {XR_TYPE_ACTION_SET_CREATE_INFO};
				verified = verifyDynamicTypeOrNull(&info2);

				CHECK(verified.get() == verifyDynamicTypeOrNull(&info2).get());

				CHECK(verified.get() == &info2);

				CHECK_FALSE(verified.get() == verifyDynamicTypeOrNull(&info).get());
				CHECK_FALSE(verified.get() == &info);
			}
		}
	}
}

TEST_CASE("tryGetFromChain")
{
	WHEN("Passing pointer to desired type")
	{
		XrActionSetCreateInfo ascInfo = {XR_TYPE_ACTION_SET_CREATE_INFO};
		THEN("Should return same non-nullptr input pointer without "
		     "throwing")
		{
			REQUIRE_NOTHROW(tryGetFromChain<XrActionSetCreateInfo>(&ascInfo));

			CHECK_FALSE(tryGetFromChain<XrActionSetCreateInfo>(&ascInfo) == nullptr);

			CHECK(tryGetFromChain<XrActionSetCreateInfo>(&ascInfo).get() == &ascInfo);
		}
	}

	WHEN("Passing reference to desired type")
	{
		XrActionSetCreateInfo ascInfo = {XR_TYPE_ACTION_SET_CREATE_INFO};
		THEN("Should return pointer to same input without "
		     "throwing")
		{
			REQUIRE_NOTHROW(tryGetFromChain<XrActionSetCreateInfo>(ascInfo));

			CHECK_FALSE(tryGetFromChain<XrActionSetCreateInfo>(ascInfo) == nullptr);

			CHECK(tryGetFromChain<XrActionSetCreateInfo>(ascInfo).get() == &ascInfo);
		}
	}

	WHEN("Passing reference to desired type initialized with "
	     "different but known type member")
	{
		XrActionSetCreateInfo wrongInfo = {XR_TYPE_INSTANCE_CREATE_INFO};
		THEN("Should return equal to nullptr")
		{
			CHECK(tryGetFromChain<XrActionSetCreateInfo>(wrongInfo) == nullptr);
			CHECK(nullptr == tryGetFromChain<XrActionSetCreateInfo>(wrongInfo));
			CHECK_FALSE(tryGetFromChain<XrActionSetCreateInfo>(wrongInfo) != nullptr);
			CHECK_FALSE(nullptr != tryGetFromChain<XrActionSetCreateInfo>(wrongInfo));
		}
	}

	WHEN("Passing reference to desired type initialized with "
	     "an unknown type member")
	{
		XrActionSetCreateInfo wrongInfo = {XR_STRUCTURE_TYPE_MAX_ENUM};
		THEN("Should return equal to nullptr")
		{
			CHECK(tryGetFromChain<XrActionSetCreateInfo>(wrongInfo) == nullptr);
			CHECK_FALSE(tryGetFromChain<XrActionSetCreateInfo>(wrongInfo) != nullptr);
		}
	}

	WHEN("Passing null XrActionSetCreateInfo pointer")
	{
		XrActionSetCreateInfo* nullCreateInfo = nullptr;
		THEN("Should return equal to nullptr")
		{
			CHECK(tryGetFromChain<XrActionSetCreateInfo>(nullCreateInfo) == nullptr);
			CHECK_FALSE(tryGetFromChain<XrActionSetCreateInfo>(nullCreateInfo) != nullptr);
		}
	}
	WHEN("A struct chains to the desired type")
	{

		XrActionSetCreateInfo ascInfo = {XR_TYPE_ACTION_SET_CREATE_INFO};
		XrInstanceCreateInfo icInfo = {XR_TYPE_INSTANCE_CREATE_INFO, &ascInfo};
		AND_WHEN("tryGetFromChain called with pointer")
		{
			THEN("Should return correct non-nullptr pointer "
			     "without throwing")
			{
				REQUIRE_NOTHROW(tryGetFromChain<XrActionSetCreateInfo>(&icInfo));

				CHECK_FALSE(tryGetFromChain<XrActionSetCreateInfo>(&icInfo) == nullptr);

				CHECK(tryGetFromChain<XrActionSetCreateInfo>(&icInfo).get() == &ascInfo);
			}
		}

		AND_WHEN("tryGetFromChain called with reference")
		{
			THEN("Should return correct non-nullptr pointer "
			     "without throwing")
			{
				REQUIRE_NOTHROW(tryGetFromChain<XrActionSetCreateInfo>(icInfo));

				CHECK_FALSE(tryGetFromChain<XrActionSetCreateInfo>(icInfo) == nullptr);

				CHECK(tryGetFromChain<XrActionSetCreateInfo>(icInfo).get() == &ascInfo);
			}
		}
		AND_WHEN("tryGetFromChain called with temporary "
		         "DynamicVerifiedOrNull")
		{
			THEN("Should return correct non-nullptr pointer "
			     "without throwing")
			{
				REQUIRE_NOTHROW(
				    tryGetFromChain<XrActionSetCreateInfo>(verifyDynamicTypeOrNull(&icInfo)));

				CHECK_FALSE(tryGetFromChain<XrActionSetCreateInfo>(verifyDynamicTypeOrNull(&icInfo)) ==
				            nullptr);

				CHECK(tryGetFromChain<XrActionSetCreateInfo>(verifyDynamicTypeOrNull(&icInfo)).get() ==
				      &ascInfo);
			}
		}
		AND_WHEN("tryGetFromChain called with temporary DynamicVerified")
		{
			THEN("Should return correct non-nullptr pointer "
			     "without throwing")
			{
				REQUIRE_NOTHROW(tryGetFromChain<XrActionSetCreateInfo>(verifyDynamicType(icInfo)));

				CHECK_FALSE(tryGetFromChain<XrActionSetCreateInfo>(verifyDynamicType(icInfo)) ==
				            nullptr);

				CHECK(tryGetFromChain<XrActionSetCreateInfo>(verifyDynamicType(icInfo)).get() ==
				      &ascInfo);
			}
		}
		AND_WHEN("tryGetFromChain called with non-temporary "
		         "DynamicVerifiedOrNull")
		{
			THEN("Should return correct non-nullptr pointer "
			     "without throwing")
			{
				auto icInfoVerified = verifyDynamicTypeOrNull(&icInfo);
				REQUIRE_NOTHROW(tryGetFromChain<XrActionSetCreateInfo>(icInfoVerified));

				CHECK_FALSE(tryGetFromChain<XrActionSetCreateInfo>(icInfoVerified) == nullptr);

				CHECK(tryGetFromChain<XrActionSetCreateInfo>(icInfoVerified).get() == &ascInfo);
			}
		}
		AND_WHEN("tryGetFromChain called with non-temporary DynamicVerified")
		{
			THEN("Should return correct non-nullptr pointer "
			     "without throwing")
			{
				auto icInfoVerified = verifyDynamicType(icInfo);
				REQUIRE_NOTHROW(tryGetFromChain<XrActionSetCreateInfo>(icInfoVerified));

				CHECK_FALSE(tryGetFromChain<XrActionSetCreateInfo>(icInfoVerified) == nullptr);

				CHECK(tryGetFromChain<XrActionSetCreateInfo>(icInfoVerified).get() == &ascInfo);
			}
		}
	}
}

// TEST_CASE("PolymorphicSpan")
// {
// 	WHEN("Passing pointer to XrActionSetCreateInfo array")
// 	{
// 		std::array<XrActionSetCreateInfo, 3> actionSetArray;
// 		auto ptr = actionSetArray.data();
// 		for (auto& elt : actionSetArray) {
// 			initXrType(elt);
// 		}
// 		XrActionSetCreateInfo actionSetCreateInfo = {XR_TYPE_ACTION_SET_CREATE_INFO};
// 		THEN("Should wrap same non-nullptr input pointer and size without throwing")
// 		{
// 			REQUIRE_NOTHROW(XRTRAITS_MAKE_POLYMORPHIC_SPAN(ptr, actionSetArray.size()));
// 			CHECK(XRTRAITS_MAKE_POLYMORPHIC_SPAN(ptr, actionSetArray.size()).data() == ptr);
// 			CHECK(XRTRAITS_MAKE_POLYMORPHIC_SPAN(ptr, actionSetArray.size()).size() == actionSetArray.size());

// 			REQUIRE_NOTHROW(make_polymorphic_span(ptr, actionSetArray.size()));
// 			CHECK(make_polymorphic_span(ptr, actionSetArray.size()).data() == ptr);
// 			CHECK(make_polymorphic_span(ptr, actionSetArray.size()).size() == actionSetArray.size());
// 		}
// 		AND_WHEN("Retrieving via correct required_verified_span_cast")
// 		{
// 			THEN("Should return pointer to same input without "
// 			     "throwing")
// 			{
// 				REQUIRE_NOTHROW(required_verified_span_cast<XrActionSetCreateInfo>(
// 				    XRTRAITS_MAKE_POLYMORPHIC_SPAN(ptr, actionSetArray.size())));

// 				auto castedSpan = required_verified_span_cast<XrActionSetCreateInfo>(
// 				    XRTRAITS_MAKE_POLYMORPHIC_SPAN(ptr, actionSetArray.size()));
// 				REQUIRE(castedSpan.data() == ptr);
// 				REQUIRE(castedSpan.size() == actionSetArray.size());
// 			}
// 		}
// 		/// @todo check error cases, such as reinterpreting to a pointer to XrSwapchainImageBaseHeader
// 	}
// }

#endif // XRTRAITS_HAVE_DYNAMIC_VERIFIED
