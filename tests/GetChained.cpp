// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*! @file
 *  @brief  Test of GetChained.h
 *  @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "xrtraits/casts/GetChained.h"

#ifdef XRTRAITS_HAVE_CASTS

// Library Includes
#include <catch2/catch.hpp>

// Standard Includes
// - none

using namespace xrtraits;
using xrtraits::casts::xr_get_chained_struct;

TEST_CASE("xr_get_chained_struct")
{
	WHEN("Passing null XrFrameState pointer")
	{
		XrFrameState* nullFrameState = nullptr;
		AND_WHEN("using an explicit template argument")
		{
			THEN("Should return nullptr")
			{
				REQUIRE_NOTHROW(xr_get_chained_struct<XrFrameState*>(nullFrameState));
				REQUIRE_NOTHROW(xr_get_chained_struct<XrFrameState*>(nullFrameState) == nullptr);
			}
		}
		AND_WHEN("not using an explicit template argument")
		{
			THEN("Should return nullptr")
			{
				REQUIRE_NOTHROW(xr_get_chained_struct(nullFrameState));
				REQUIRE_NOTHROW(xr_get_chained_struct(nullFrameState) == nullptr);
			}
		}
	}
	WHEN("A struct chains to the desired type")
	{
		XrFrameState frameState = {XR_TYPE_FRAME_STATE};
		XrInstanceCreateInfo icInfo = {XR_TYPE_INSTANCE_CREATE_INFO, &frameState};
		AND_WHEN("xr_get_chained_struct called with explicit template argument")
		{
			THEN("Should return correct non-nullptr pointer "
			     "without throwing")
			{
				REQUIRE_NOTHROW(xr_get_chained_struct<XrFrameState*>(&icInfo));

				CHECK_FALSE(xr_get_chained_struct<XrFrameState*>(&icInfo) == nullptr);

				CHECK(xr_get_chained_struct<XrFrameState*>(&icInfo) == &frameState);
			}
		}
		AND_WHEN("xr_get_chained_struct called with no explicit template argument")
		{
			THEN("Should return correct non-nullptr pointer "
			     "without throwing")
			{
				REQUIRE_NOTHROW(xr_get_chained_struct(&icInfo));

				CHECK_FALSE(xr_get_chained_struct(&icInfo) == nullptr);

				CHECK(xr_get_chained_struct(&icInfo) == &icInfo);
			}
		}
	}
	WHEN("Desired type not in the chain")
	{
		XrFrameState frameState = {XR_TYPE_ACTION_SET_CREATE_INFO};
		XrInstanceCreateInfo icInfo = {XR_TYPE_INSTANCE_CREATE_INFO, &frameState};
		THEN("Should return nullptr")
		{
			REQUIRE_NOTHROW(xr_get_chained_struct<XrActionCreateInfo*>(&icInfo));

			CHECK(xr_get_chained_struct<XrActionCreateInfo*>(&icInfo) == nullptr);
		}
	}
}

#endif // XRTRAITS_HAVE_CASTS
