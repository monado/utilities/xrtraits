// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  C++ type traits related to OpenXR, and some other compile-time
 * functionality.
 *
 * Includes a generated header that defines most of these based on the registry.
 *
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wswitch-enum"
#endif

#include "xrtraits/traits/APITraitsImpl.h" // Generated header

#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif

#include "xrtraits/Common.h"

// Library Includes
#include <openxr/openxr.h>
#include <openxr/openxr_reflection.h>

// Standard Includes
#include <type_traits>
#include <utility>

namespace xrtraits {

namespace traits {

#ifndef XRTRAITS_DOXYGEN
	namespace detail {
		using std::remove_const_t;
		using std::remove_cv_t;
		using std::remove_pointer_t;
		using std::remove_reference_t;
		/*! Removes reference and pointer qualifications (in order,
		 * outside-in).
		 */
		template <typename T>
		using remove_reference_pointer_t =
		    remove_pointer_t<remove_reference_t<T>>;
		/*! Removes reference, pointer, and const/volatile
		 * qualifications (in order, outside-in).
		 */
		template <typename T>
		using remove_reference_pointer_cv_t =
		    remove_cv_t<remove_reference_pointer_t<T>>;

		template <typename T>
		constexpr bool has_const_next_pointer_v =
		    std::is_const<remove_pointer_t<decltype(T::next)>>::value;

#define XRTRAITS_MAKE_STRINGIFY_CASE(SYM, _)                                   \
	case SYM:                                                              \
		return #SYM;
		constexpr const char*
		structureTypeEnumToString(XrStructureType tag)
		{
			// Awkward workaround: we shouldn't stringify the
			// MAX_ENUM
			if (tag == XR_STRUCTURE_TYPE_MAX_ENUM) {
				return nullptr;
			}
			switch (tag) {

				XR_LIST_ENUM_XrStructureType(
				    XRTRAITS_MAKE_STRINGIFY_CASE);
			default:
				return nullptr;
			}
		}
#undef XRTRAITS_MAKE_STRINGIFY_CASE
	} // namespace detail

#endif // !XRTRAITS_DOXYGEN

	/*! true if the given type is an XR tagged type (starting with
	 * `XrStructureType type; const void* next;`).
	 *
	 * Note that this does *not* imply that there is a (known)
	 * XrStructureType enum value (for use in the `type` member) that
	 * corresponds to T. For that, @see has_xr_type_tag_v
	 *
	 * Removes pointer, reference, and const from type before looking it
	 * up.
	 */
	template <typename T>
	constexpr bool is_xr_tagged_type_v = detail::is_xr_tagged_type_impl_v<
	    detail::remove_reference_pointer_cv_t<T>>;

	/*! For types that are XR tagged types and have a defined enum value
	 * (type tag) for their type member, this is true.
	 *
	 * Note: Not all types with is_xr_tagged_type_v have their own type tag:
	 * some (that primarily serve as "abstract" types) do not but are
	 * intended primarily as ways to pass references to concrete types that
	 * do have a defined type tag.
	 *
	 * Removes pointer, reference, and const from type before looking it
	 * up.
	 */
	template <typename T>
	constexpr bool has_xr_type_tag_v = detail::has_xr_type_tag_impl_v<
	    detail::remove_reference_pointer_cv_t<T>>;

	/*! Access the defined enum value (type tag) for the type member of an
	 * XR tagged type with known tag.
	 *
	 * Removes pointer, reference, and const from type before looking it
	 * up.
	 */
	template <typename T>
	constexpr XrStructureType xr_type_tag_v = detail::xr_type_tag_impl_v<
	    detail::remove_reference_pointer_cv_t<T>>;

	/*! Identify if a tagged type has a const next pointer.
	 *
	 * Removes pointer, reference, and const from type before looking it
	 * up.
	 */
	template <typename T>
	constexpr bool has_const_next_pointer_v =
	    detail::has_const_next_pointer_v<
	        detail::remove_reference_pointer_cv_t<T>>;

	/*! Get a string corresponding to a type enum value, or nullptr if not
	 * recognized.
	 */
	constexpr const char* to_string(XrStructureType t)
	{
		return detail::structureTypeEnumToString(t);
	}

	/*! Get the type name string associated with a type enum value, or
	 * nullptr if not recognized.
	 */
	constexpr const char* getTypeName(XrStructureType t)
	{
		return detail::structureTypeEnumToTypeNameString(t);
	}

	//! Do SourceType and TargetType have the same const-qualifications?
	template <typename SourceType, typename TargetType>
	constexpr bool same_constness = (is_const_v<SourceType> ==
	                                 is_const_v<TargetType>);

} // namespace traits

} // namespace xrtraits
