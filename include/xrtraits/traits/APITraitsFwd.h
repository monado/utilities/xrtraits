// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header with forward declarations/default specializations of functions
 * and traits defined/specialized by the generated file APITraitsImpl.h, which
 * includes this one.
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// IWYU pragma: private, include "xrtraits/APITraits.h"
// IWYU pragma: friend "APITraitsImpl\.h"

#pragma once

// Internal Includes
// - none

// Library Includes
#include <openxr/openxr.h>

// Standard Includes
#include <cstddef>

namespace xrtraits {
namespace traits {

#ifndef XRTRAITS_DOXYGEN
	namespace detail {
		/*! Declaration/default implementation of the actual trait
		 * used by is_xr_tagged_type_v - specializations are generated.
		 */
		template <typename T>
		constexpr bool is_xr_tagged_type_impl_v = false;

		/*! Is true for types that are XR tagged types and have a
		 * defined enum value (type tag) for their type member, false
		 * otherwise.
		 *
		 * Not all types with is_xr_tagged_type_v have their own type
		 * tag: some (that primarily serve as "abstract" types, often
		 * named `...BaseHeader`) do not but are intended primarily as
		 * ways to pass references to concrete types that do have a
		 * defined type tag.
		 */
		template <typename T>
		constexpr bool has_xr_type_tag_impl_v = false;

		/*! For types that are XR tagged types and have a defined enum
		 * value (type tag) for their type member, defines a
		 * XrStructureType.
		 *
		 * Default value is nullptr in order to break if it shouldn't
		 * be used with a given type.
		 */
		template <typename T>
		constexpr std::nullptr_t xr_type_tag_impl_v = nullptr;

		/*! A string literal with the type name for all known XR
		 * structure types.
		 */
		template <typename T>
		constexpr std::nullptr_t xr_type_name_impl_v = nullptr;

		/*! A string literal with the type name for all XR types with
		 * known tags.
		 */
		template <XrStructureType Tag>
		constexpr std::nullptr_t xr_type_name_by_tag_impl_v = nullptr;

	} // namespace detail

#endif // !XRTRAITS_DOXYGEN

	/*! A struct template to wrap a type, to be able to pass it as
	 * an argument.
	 */
	template <typename T> struct TypeWrapper
	{
		//! The type that we're wrapping.
		using type = T;

		// Default constructor, constexpr to permit usage in constexpr
		// functions.
		constexpr TypeWrapper() = default;
	};

	/*! Calls your functor with a TypeWrapper<T> argument, where T is the
	 * static type matching the enum value.
	 *
	 * If the enum value is not recognized, your functor will not be called.
	 */
	template <typename F>
	constexpr void typeDispatch(XrStructureType t, F&& f);
} // namespace traits
} // namespace xrtraits
