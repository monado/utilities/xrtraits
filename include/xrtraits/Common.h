// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header with common utilities used by multiple headers
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
// - none

// Library Includes
// - none

// Standard Includes
#include <type_traits>

namespace xrtraits {

//! Variable template wrapping std::is_pointer<T>::value
template <typename T> constexpr bool is_pointer_v = std::is_pointer<T>::value;

//! Variable template wrapping std::is_reference<T>::value
template <typename T>
constexpr bool is_reference_v = std::is_reference<T>::value;

//! Variable template wrapping std::is_const<T>::value
template <typename T> constexpr bool is_const_v = std::is_const<T>::value;

//! Variable template wrapping std::is_same<T, U>::value
template <typename T, typename U>
constexpr bool is_same_v = std::is_same<T, U>::value;

} // namespace xrtraits



#if (__cplusplus >= 201703L) && !defined(XRTRAITS_HAVE_CXX17)
#define XRTRAITS_HAVE_CXX17
#endif

#if defined(XRTRAITS_HAVE_CXX17) && !defined(XRTRAITS_HAVE_OPTIONAL)
#define XRTRAITS_HAVE_OPTIONAL
#endif

#if defined(XRTRAITS_HAVE_CXX17) && !defined(XRTRAITS_HAVE_CONSTEXPR_IF)
#define XRTRAITS_HAVE_CONSTEXPR_IF
#endif
