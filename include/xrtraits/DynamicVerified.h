// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header providing type-enforced verification of OpenXR "tagged types".
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

#include "Common.h"

// Needs exceptions and constexpr-if
#if defined(XRTRAITS_HAVE_EXCEPTIONS) && defined(XRTRAITS_HAVE_CONSTEXPR_IF)

/*!
 * Defined after including DynamicVerified.h if the compiler and config
 * requirements are satisfied to provide at least some DynamicVerified functionality.
 */
#define XRTRAITS_HAVE_DYNAMIC_VERIFIED

// Internal Includes
#include "Attributes.h"
#include "Common.h"
#include "casts/GetChained.h"
#include "casts/TaggedDynamicCast.h"
#include "casts/TaggedRiskyCast.h"
#include "exceptions/TypeError.h"
#include "traits/APITraits.h"

// Library Includes
#include "GSLWrap.h"
#include <openxr/openxr.h>

// Standard Includes
#include <functional>

#ifdef XRTRAITS_DOXYGEN
/*! If defined, DynamicVerified re-checks the type member of the struct it wraps
 * on every access, throwing in case of a mismatch.
 *
 * @relates xrtraits::DynamicVerified
 */
#define XRTRAITS_DYNAMIC_VERIFIED_ALWAYS_RECHECK
#endif // XRTRAITS_DOXYGEN

#ifdef XRTRAITS_DYNAMIC_VERIFIED_ALWAYS_RECHECK
/*! noexcept if XRTRAITS_DYNAMIC_VERIFIED_ALWAYS_RECHECK is not defined, empty
 * otherwise
 *
 * @relates xrtraits::DynamicVerified
 */
#define XRTRAITS_NOEXCEPT_IF_NOT_RECHECKING
#else
#define XRTRAITS_NOEXCEPT_IF_NOT_RECHECKING noexcept
#endif

namespace xrtraits {

/*! A wrapper for OpenXR types that have had their `type` member's value (and
 * thus their "dynamic type") checked, or are a null pointer.
 *
 * Acts like a pointer to T - can compare to nullptr, use -> to access members
 * of T, *thing to dereference... Is also sized the exact same as a raw
 * pointer, so no space overhead.
 *
 * Pass by value, not by pointer or reference.
 *
 * Typically created with one of the following free functions:
 *
 * - verifyDynamicTypeOrNull() when you care about head of the chain and
 *   the dynamic type matching the pre-existing static type.
 * - XRTRAITS_VERIFY_DYNAMIC_TYPE_OR_NULL() macro wrapping the above,
 * stringizing its argument for better error messages when used in entry points.
 * - tryGetFromChain<T>() when you are looking anywhere in the chain if it
 *   happens to be non-null and if it happens to include the desired type.
 * - required_verified_or_null_cast<T>() when you are looking at a
 *   (possibly null) pointer with a static type that you don't want and that
 *   you know doesn't match its dynamic type.
 *
 * @see DynamicVerified The "stronger" version of this type, for passing
 * required parameters.
 *
 * @ingroup TypeVerification
 */
template <typename T> class DynamicVerifiedOrNull;

/*! A wrapper for OpenXR types that have had their `type` member's value (and
 * thus their "dynamic type") checked.
 *
 * Acts like a pointer to T - can use -> to access members of T, *thing to
 * dereference, etc. (Can compare to nullptr too, but this is never equal to
 * nullptr.)
 *
 * Is also sized the exact same as a raw pointer, so no space overhead.
 *
 * Pass by value, not by pointer or reference.
 *
 * Inherits from DynamicVerifiedOrNull, because it provides a strict superset
 * of the guarantees of DynamicVerifiedOrNull.
 *
 * Note this is just an observer and does **not** convey ownership.
 *
 * Typically created with one of the following free functions:
 *
 * - verifyDynamicType() when you care about head of the chain and
 *   the dynamic type matching the pre-existing static type.
 * - XRTRAITS_VERIFY_DYNAMIC_TYPE() macro wrapping the above, stringizing its
 *   argument for better error messages when used in entry points.
 * - getFromChain<T>() when you are looking anywhere in the chain.
 * - required_verified_cast<T>() when you are looking at a pointer with a
 *   static type that you don't want and that you know doesn't match its
 *   dynamic type.
 * - ensureNotNull() to upgrade a DynamicVerifiedOrNull.
 *
 * @see DynamicVerifiedOrNull The "weaker" version of this type, for passing
 * optional parameters.
 *
 * @ingroup TypeVerification
 */
template <typename T> class DynamicVerified;

/*! Check that either nullptr was passed, or that the type member's value
 * (dynamic type) of a struct matches its static type as expected.
 *
 * This is the weaker relative of verifyDynamicType(), for optional parameters.
 *
 * @throw exceptions::type_error if type member has mis-matched value for
 * non-nullptr argument.
 *
 * @param param Possibly-null pointer to an XR tagged type with known tag
 * @param paramName Parameter name as defined in API, if known (optional).
 * @param errorCode Error result code to include in exception if this throws
 * (optional, defaults to validation failure).
 *
 * @return A wrapper for a reference to the supplied struct, to indicate
 * statically that the dynamic type has been verified or that it is null.
 *
 * @ingroup TypeVerification
 * @relatesalso DynamicVerifiedOrNull
 */
template <typename T>
XRTRAITS_NODISCARD constexpr DynamicVerifiedOrNull<T> verifyDynamicTypeOrNull(
    T* const& param, const char* paramName = nullptr,
    XrResult errorCode = exceptions::DEFAULT_TYPE_ERROR_CODE);

/*! Check that the type member's value (dynamic type) of a struct matches its
 * static type as expected (pointer overload).
 *
 * This is the stronger relative of verifyDynamicTypeOrNull(), for non-optional
 * parameters.
 *
 * @throw exceptions::type_error if nullptr is passed or type member has
 * mis-matched value.
 *
 * @param param Pointer to an XR tagged type with known tag
 * @param paramName Parameter name as defined in API, if known (optional).
 * @param errorCode Error result code to include in exception if this throws
 * (optional, defaults to validation failure).
 *
 * @return A wrapper for a reference to the supplied struct, to indicate
 * statically that the dynamic type has been verified.
 *
 * @ingroup TypeVerification
 * @relatesalso DynamicVerified
 */
template <typename T>
XRTRAITS_NODISCARD constexpr DynamicVerified<T>
verifyDynamicType(T* param, const char* paramName = nullptr,
                  XrResult errorCode = exceptions::DEFAULT_TYPE_ERROR_CODE);

/*! Check that the type member's value (dynamic type) of a struct matches its
 * static type as expected (reference overload).
 *
 * This is the stronger relative of verifyDynamicTypeOrNull(), for non-optional
 * parameters.
 *
 * @throw exceptions::type_error if type member has mis-matched value.
 *
 * @param param Reference to an XR tagged type with known tag
 * @param paramName Parameter name as defined in API, if known (optional).
 * @param errorCode Error result code to include in exception if this throws
 * (optional, defaults to validation failure).
 *
 * @return A wrapper for a reference to the supplied struct, to indicate
 * statically that the dynamic type has been verified.
 *
 * @ingroup TypeVerification
 * @relatesalso DynamicVerified
 */
template <typename T>
XRTRAITS_NODISCARD constexpr DynamicVerified<T>
verifyDynamicType(T& param, const char* paramName = nullptr,
                  XrResult errorCode = exceptions::DEFAULT_TYPE_ERROR_CODE);

/*! Re-interpret the given pointer as a DynamicVerified of the supplied type, as
 * long as the dynamic type matches.
 *
 * Related to verifyDynamicType(), but this one requires a type parameter to be
 * explicitly specified.
 *
 * @throw exceptions::type_error if nullptr is passed or type member has
 * mis-matched value.
 *
 * @tparam T OpenXR type with known tag to try casting to.
 * @param param pointer to some XR tagged type, or nullptr
 * @param paramName Parameter name as defined in API, if known (optional).
 * @param errorCode Error result code to include in exception if this throws
 * (optional, defaults to validation failure).
 *
 * @return A wrapper for a reference to the supplied struct, to indicate
 * statically that the dynamic type has been verified.
 *
 * @ingroup TypeVerification
 * @relatesalso DynamicVerified
 */
template <typename T, typename U>
XRTRAITS_NODISCARD constexpr DynamicVerified<T> required_verified_cast(
    U* const& param, const char* paramName = nullptr,
    XrResult errorCode = exceptions::DEFAULT_TYPE_ERROR_CODE);

/*! Re-interpret the given reference as a DynamicVerified of the supplied type,
 * as long as the dynamic type matches.
 *
 * Related to verifyDynamicType(), but this one requires a type parameter to be
 * explicitly specified.
 *
 * @throw exceptions::type_error if type member has
 * mis-matched value.
 *
 * @tparam T OpenXR type with known tag to try casting to.
 * @param param reference to some XR tagged type
 * @param paramName Parameter name as defined in API, if known (optional).
 * @param errorCode Error result code to include in exception if this throws
 * (optional, defaults to validation failure).
 *
 * @return A wrapper for a reference to the supplied struct, to indicate
 * statically that the dynamic type has been verified.
 *
 * @ingroup TypeVerification
 * @relatesalso DynamicVerified
 */
template <typename T, typename U>
XRTRAITS_NODISCARD constexpr DynamicVerified<T> required_verified_cast(
    U& param, const char* paramName = nullptr,
    XrResult errorCode = exceptions::DEFAULT_TYPE_ERROR_CODE);

/*! Re-interpret the given pointer as a DynamicVerifiedOrNull of the supplied
 * type, as long as the dynamic type matches or the input is nullptr.
 *
 * Related to verifyDynamicType(), but this one requires a type parameter to be
 * explicitly specified.
 *
 * @throw exceptions::type_error if non-null and type member has mis-matched
 * value.
 *
 * @tparam T OpenXR type with known tag to try casting to.
 * @param param pointer to some XR tagged type, or nullptr
 * @param paramName Parameter name as defined in API, if known (optional).
 * @param errorCode Error result code to include in exception if this throws
 * (optional, defaults to validation failure).
 *
 * @ingroup TypeVerification
 * @relatesalso DynamicVerified
 */
template <typename T, typename U>
XRTRAITS_NODISCARD constexpr DynamicVerifiedOrNull<T>
required_verified_or_null_cast(
    U* param, const char* paramName = nullptr,
    XrResult errorCode = exceptions::DEFAULT_TYPE_ERROR_CODE);

/*! Upgrade DynamicVerifiedOrNull to DynamicVerified.
 *
 * @throw exceptions::type_error if input is nullptr.
 *
 * @param param DynamicVerifiedOrNull<T> object referencing something non-null.
 * @param paramName Parameter name as defined in API, if known (optional).
 * @param errorCode Error result code to include in exception if this throws
 * (optional, defaults to validation failure).
 *
 * @return A DynamicVerified wrapper for a reference to the supplied struct, to
 * indicate statically that the dynamic type has been verified.
 *
 * @ingroup TypeVerification
 * @relates DynamicVerifiedOrNull
 */
template <typename T>
XRTRAITS_NODISCARD constexpr DynamicVerified<T>
ensureNotNull(DynamicVerifiedOrNull<T> const& param,
              const char* paramName = nullptr,
              XrResult errorCode = exceptions::DEFAULT_TYPE_ERROR_CODE);

/*! Given an OpenXR structure type, get another type from its next chain -
 * pointer head overload.
 *
 * @throw exceptions::get_from_chain_error (derived from
 * exceptions::type_error) if no struct of the right type exists on the chain.
 *
 * @param param Head of the chain
 * @param paramName Parameter name as defined in API, if known (optional).
 * @param errorCode Error result code to include in exception if this throws
 * (optional, defaults to validation failure).
 *
 * @return The desired struct, wrapped in DynamicVerified to indicate
 * statically that the dynamic type has been verified.
 *
 * @ingroup TypeVerification
 * @relatesalso DynamicVerified
 */
template <typename TargetType, typename SourceType>
XRTRAITS_NODISCARD constexpr DynamicVerified<TargetType>
getFromChain(SourceType* param, const char* paramName = nullptr,
             XrResult errorCode = exceptions::DEFAULT_TYPE_ERROR_CODE);

/*! Given an OpenXR structure type, get another type from its next chain -
 * reference head overload.
 *
 * @throw exceptions::get_from_chain_error (derived from
 * exceptions::type_error) if no struct of the right type exists on the chain.
 *
 * @param param Head of the chain
 * @param paramName Parameter name as defined in API, if known (optional).
 * @param errorCode Error result code to include in exception if this throws
 * (optional, defaults to validation failure).
 *
 * @return The desired struct, wrapped in DynamicVerified to indicate
 * statically that the dynamic type has been verified.
 *
 * @ingroup TypeVerification
 * @relatesalso DynamicVerified
 */
template <typename TargetType, typename SourceType>
XRTRAITS_NODISCARD constexpr std::enable_if_t<
    traits::is_xr_tagged_type_v<SourceType>, DynamicVerified<TargetType>>
getFromChain(SourceType& param, const char* paramName = nullptr,
             XrResult errorCode = exceptions::DEFAULT_TYPE_ERROR_CODE);

/*! Given an OpenXR structure type, get another type from its next chain -
 * DynamicVerified head overload.
 *
 * @throw exceptions::get_from_chain_error (derived from
 * exceptions::type_error) if no struct of the right type exists on the chain.
 *
 * @param param Head of the chain
 * @param paramName Parameter name as defined in API, if known (optional).
 * @param errorCode Error result code to include in exception if this throws
 * (optional, defaults to validation failure).
 *
 * @return The desired struct, wrapped in DynamicVerified to indicate
 * statically that the dynamic type has been verified.
 *
 * @ingroup TypeVerification
 * @relatesalso DynamicVerified
 */
template <typename TargetType, typename SourceType>
XRTRAITS_NODISCARD constexpr DynamicVerified<TargetType>
getFromChain(DynamicVerified<SourceType> const& param,
             const char* paramName = nullptr,
             XrResult errorCode = exceptions::DEFAULT_TYPE_ERROR_CODE);

/*! Given an OpenXR structure type, try to get another type from its next chain
 * - pointer head overload.
 *
 * @param param Head of the chain (may be nullptr)
 *
 * @return The desired struct if found, wrapped in DynamicVerifiedOrNull to
 * indicate statically that the dynamic type has been verified, or a
 * DynamicVerifiedOrNull initialized with nullptr if not found.
 *
 * @return The desired struct if found, wrapped in DynamicVerifiedOrNull to
 * indicate statically that the dynamic type has been verified, or a
 * DynamicVerifiedOrNull initialized with nullptr if not found.
 *
 * @ingroup TypeVerification
 * @relatesalso DynamicVerifiedOrNull
 */
template <typename TargetType, typename SourceType>
XRTRAITS_NODISCARD constexpr DynamicVerifiedOrNull<TargetType>
tryGetFromChain(SourceType* param) noexcept;

/*! Given an OpenXR structure type, try to get another type from its next chain
 * - reference head overload.
 *
 * @param param Head of the chain
 *
 * @return The desired struct if found, wrapped in DynamicVerifiedOrNull to
 * indicate statically that the dynamic type has been verified, or a
 * DynamicVerifiedOrNull initialized with nullptr if not found.
 *
 * @ingroup TypeVerification
 * @relatesalso DynamicVerifiedOrNull
 */
template <typename TargetType, typename SourceType>
XRTRAITS_NODISCARD constexpr std::enable_if_t<
    traits::is_xr_tagged_type_v<SourceType>, DynamicVerifiedOrNull<TargetType>>
tryGetFromChain(SourceType& param) noexcept;

/*! Given an OpenXR structure type, try to get another type from its next chain
 * - DynamicVerified head overload.
 *
 * @param param Head of the chain (may be nullptr)
 *
 * @return The desired struct if found, wrapped in DynamicVerifiedOrNull to
 * indicate statically that the dynamic type has been verified, or a
 * DynamicVerifiedOrNull initialized with nullptr if not found.
 *
 * @ingroup TypeVerification
 * @relatesalso DynamicVerifiedOrNull
 */
template <typename TargetType, typename SourceType>
XRTRAITS_NODISCARD inline constexpr DynamicVerifiedOrNull<TargetType>
tryGetFromChain(DynamicVerifiedOrNull<SourceType> const& param) noexcept;

} // namespace xrtraits

/*! Wrapper for xrtraits::verifyDynamicType() that stringizes its argument.
 *
 * @ingroup TypeVerification
 * @relatesalso xrtraits::DynamicVerified
 */
#define XRTRAITS_VERIFY_DYNAMIC_TYPE(PARAM)                                    \
	::xrtraits::verifyDynamicType(PARAM, #PARAM)

/*! Wrapper for xrtraits::verifyDynamicTypeOrNull() that stringizes its
 * argument.
 *
 * @ingroup TypeVerification
 * @relatesalso xrtraits::DynamicVerifiedOrNull
 */
#define XRTRAITS_VERIFY_DYNAMIC_TYPE_OR_NULL(PARAM)                            \
	::xrtraits::verifyDynamicTypeOrNull(PARAM, #PARAM)

namespace xrtraits {

/*! Behaves like a gsl::span<T> except that it enforces that the dynamic type of
 * each array member matches the static type.
 */
template <typename T> class DynamicVerifiedSpan;

/*! Similar to gsl::make_span, but the resulting span has its dynamic type
 * verified.
 */
template <typename T>
XRTRAITS_NODISCARD inline constexpr DynamicVerifiedSpan<T>
make_dynamic_verified_span(
    T* pointer, std::ptrdiff_t size, const char* paramName = nullptr,
    XrResult errorCode = exceptions::DEFAULT_TYPE_ERROR_CODE);

} // namespace xrtraits

/*! Wrapper for xrtraits::make_polymorphic_span() that stringizes its pointer
 * argument.
 *
 * @ingroup TypeVerification
 * @relatesalso xrtraits::PolymorphicSpan
 */
#define XRTRAITS_MAKE_POLYMORPHIC_SPAN(PARAM, SIZE)                            \
	::xrtraits::make_polymorphic_span(PARAM, SIZE, #PARAM)

/*! Wrapper for xrtraits::make_dynamic_verified_span() that stringizes its
 * pointer argument.
 *
 * @ingroup TypeVerification
 * @relatesalso xrtraits::DynamicVerifiedSpan
 */
#define XRTRAITS_MAKE_DYNAMIC_VERIFIED_SPAN(PARAM, SIZE)                       \
	::xrtraits::make_dynamic_verified_span(PARAM, SIZE, #PARAM)

namespace xrtraits {
#ifndef XRTRAITS_DOXYGEN
namespace detail {
	/*! Tag type indicating that we want to look for a given type anywhere
	 * in the chain
	 */
	struct GetFromChain_t
	{
		constexpr explicit GetFromChain_t() = default;
	};

	/*! Tag argument indicating that we want to look for a given type
	 * anywhere in the chain.
	 */
	constexpr GetFromChain_t get_from_chain{};

	/*! Function used internally in the get-from-chain constructor of
	 * DynamicVerified.
	 */
	template <typename T, typename U>
	inline constexpr T& getFromChainImpl(U* param, const char* paramName,
	                                     XrResult errorCode)
	{
		static_assert(
		    traits::is_xr_tagged_type_v<U>,
		    "Can only use getFromChain() on types known to have type "
		    "and next members.");

		if (nullptr == param) {
			throw exceptions::get_from_chain_error(
			    paramName, nullptr, traits::xr_type_tag_v<T>,
			    errorCode);
		}
		auto* typed = casts::xr_get_chained_struct<T*>(param);
		if (nullptr == typed) {
			throw exceptions::get_from_chain_error(
			    paramName, param->type, traits::xr_type_tag_v<T>,
			    errorCode);
		}
		return *typed;
	}
	/*! Function used internally in the try-get-from-chain constructor of
	 * DynamicVerifiedOrNull.
	 */
	template <typename T, typename U>
	inline constexpr T* tryGetFromChainImpl(U* param) noexcept
	{
		static_assert(traits::is_xr_tagged_type_v<U>,
		              "Can only use tryGetFromChain() on types known "
		              "to have type and next members.");
		static_assert(
		    !is_pointer_v<T>,
		    "Type parameter to tryGetFromChain() should just be the "
		    "structure type, not a pointer type.");
		static_assert(
		    !is_reference_v<T>,
		    "Type parameter to tryGetFromChain() should just be the "
		    "structure type, not a reference type.");

		if (nullptr == param) {
			return nullptr;
		}

		auto* typed = casts::xr_get_chained_struct<T*>(param);

		return typed;
	}
} // namespace detail

#endif // !XRTRAITS_DOXYGEN

template <typename T> class DynamicVerifiedOrNull
{
public:
	static_assert(
	    traits::has_xr_type_tag_v<T>,
	    "Can only use verifyDynamicTypeOrNull and DynamicVerifiedOrNull on "
	    "types with an associated XrStructureType enum value.");

	/*! Constructor from pointer to wrapped type.
	 *
	 * If this directly corresponds to a consumer-supplied parameter, pass
	 * the parameter name for nicer error messages.
	 *
	 * @note Prefer calling xrtraits::verifyDynamicTypeOrNull() to create
	 * objects of this type.
	 */
	constexpr DynamicVerifiedOrNull(T* param, const char* paramName,
	                                XrResult errorCode)
	    : wrapped_(param)
	{
		if ((wrapped_ != nullptr) &&
		    (wrapped_->type != traits::xr_type_tag_v<T>)) {
			throw exceptions::type_error(paramName, wrapped_->type,
			                             traits::xr_type_tag_v<T>,
			                             errorCode);
		}
	}

	/*! Constructor from reference to wrapped type.
	 *
	 * If this directly corresponds to a consumer-supplied parameter, pass
	 * the parameter name for nicer error messages.
	 *
	 * @note Prefer calling xrtraits::verifyDynamicTypeOrNull() to create
	 * objects of this type.
	 */
	constexpr DynamicVerifiedOrNull(T& param, const char* paramName,
	                                XrResult errorCode)
	    : DynamicVerifiedOrNull(std::addressof(param), paramName, errorCode)
	{}

	/*! Constructor from nullptr
	 *
	 * @note Prefer calling xrtraits::verifyDynamicTypeOrNull() to create
	 * objects of this type.
	 */
	constexpr DynamicVerifiedOrNull(std::nullptr_t /*unused*/) noexcept
	    : wrapped_(nullptr)
	{}

	/*! "Private" constructor from the head of a chain, when we just want
	 * one member of the chain if it exists.
	 *
	 * @note Prefer calling xrtraits::tryGetFromChain() instead of this
	 * constructor directly.
	 */
	template <typename U>
	constexpr DynamicVerifiedOrNull(
	    U* param, detail::GetFromChain_t const& /*tag*/) noexcept
	    : DynamicVerifiedOrNull(detail::tryGetFromChainImpl<T>(param))
	{}

	//! Copy-constructible.
	constexpr DynamicVerifiedOrNull(DynamicVerifiedOrNull const&) noexcept =
	    default;

	//! Copy-assignable.
	constexpr DynamicVerifiedOrNull&
	operator=(DynamicVerifiedOrNull const&) noexcept = default;

	//! Move-constructible.
	constexpr DynamicVerifiedOrNull(DynamicVerifiedOrNull&& other) noexcept
	    : DynamicVerifiedOrNull(nullptr)
	{
		std::swap(wrapped_, other.wrapped_);
	}

	//! Move-assignable.
	constexpr DynamicVerifiedOrNull&
	operator=(DynamicVerifiedOrNull&& other) noexcept
	{
		wrapped_ = nullptr;
		std::swap(wrapped_, other.wrapped_);
		return *this;
	}

	// Destructor is default.
	~DynamicVerifiedOrNull() = default;

	/*! Conversion constructor between unrelated types deleted.
	 *
	 * This improves the clarity of error messages if you have a type
	 * mismatch - says you call an explicitly-deleted constructor, instead
	 * of listing all the possible constructors and why they aren't right.
	 *
	 * If you get an error on the line below, you're trying to pass
	 * DynamicVerifiedOrNull<U> as DynamicVerifiedOrNull<T> where T and U
	 * are different types. That's not allowed because it doesn't make
	 * sense in the OpenXR object model.
	 */
	template <typename U>
	DynamicVerifiedOrNull(DynamicVerifiedOrNull<U> const&) = delete;

	//! True if we contain a typed pointer, not nullptr.
	constexpr bool valid() const { return wrapped_ != nullptr; }

	//! Retrieve the pointer.
	constexpr T* get() const
	{
		if (!valid()) {
			throw exceptions::xr_logic_error(
			    "Used a DynamicVerifiedOrNull without checking to "
			    "see if it was null");
		}
#ifdef XRTRAITS_DYNAMIC_VERIFIED_ALWAYS_RECHECK
		if (wrapped_->type != traits::xr_type_tag_v<T>) {
			throw exceptions::xr_logic_error(
			    "Something changed the .type member of an OpenXR "
			    "struct after a DynamicVerifiedOrNull wrapper was "
			    "created for it.");
		}
#endif // XRTRAITS_DYNAMIC_VERIFIED_ALWAYS_RECHECK
		return wrapped_;
	}

	/*! "Smart Pointer" operator: for `p->memberName` as if this was just a
	 * raw pointer to the wrapped struct.
	 */
	constexpr T* operator->() const { return get(); }

	/*! Dereference operator: for `*p` as if this was just a raw pointer
	 * to the wrapped struct.
	 */
	constexpr T& operator*() const { return *get(); }

	/*! Accessor for wrapped pointer that does not check or throw - for
	 * internal use.
	 */
	T* uncheckedGet() const noexcept { return wrapped_; }

protected:
	/*! Private constructor that skips verification of dynamic type, for
	 * use within this file's implementation only when we've already done
	 * this verification.
	 */
	explicit constexpr DynamicVerifiedOrNull(T* wrapped) noexcept
	    : wrapped_(wrapped)
	{
		GSL_ASSUME((wrapped == nullptr) ||
		           (wrapped->type == traits::xr_type_tag_v<T>));
	}

private:
	//! Wrapped pointer.
	T* wrapped_;
};

/*! Equality comparison operator between DynamicVerifiedOrNull and nullptr.
 *
 * @relates DynamicVerifiedOrNull
 */
template <typename T>
inline constexpr bool operator==(DynamicVerifiedOrNull<T> const& wrapper,
                                 std::nullptr_t /*unused*/)
{
	return !wrapper.valid();
}

/*! Equality comparison operator between DynamicVerifiedOrNull and nullptr.
 *
 * @relates DynamicVerifiedOrNull
 */
template <typename T>
inline constexpr bool operator==(std::nullptr_t /*unused*/,
                                 DynamicVerifiedOrNull<T> const& wrapper)
{
	return !wrapper.valid();
}

/*! Inequality comparison operator between DynamicVerifiedOrNull and nullptr.
 *
 * @relates DynamicVerifiedOrNull
 */
template <typename T>
inline constexpr bool operator!=(DynamicVerifiedOrNull<T> const& wrapper,
                                 std::nullptr_t /*unused*/)
{
	return wrapper.valid();
}

/*! Inequality comparison operator between DynamicVerifiedOrNull and nullptr.
 *
 * @relates DynamicVerifiedOrNull
 */
template <typename T>
inline constexpr bool operator!=(std::nullptr_t /*unused*/,
                                 DynamicVerifiedOrNull<T> const& wrapper)
{
	return wrapper.valid();
}

template <typename T> class DynamicVerified : public DynamicVerifiedOrNull<T>
{
	using Base = DynamicVerifiedOrNull<T>;

public:
	static_assert(traits::has_xr_type_tag_v<T>,
	              "Can only use verifyDynamicType and DynamicVerified on "
	              "types with an associated XrStructureType enum value.");
	static_assert(!is_pointer_v<T>,
	              "Can't specify a pointer type as the type parameter to "
	              "DynamicVerified.");
	static_assert(!is_reference_v<T>,
	              "Can't specify a pointer type as the type parameter to "
	              "DynamicVerified.");

	/*! Constructor from reference to wrapped type.
	 *
	 * If this directly corresponds to a consumer-supplied parameter, pass
	 * the parameter name for nicer error messages.
	 *
	 * @note Prefer calling xrtraits::verifyDynamicType() to create objects
	 * of this type.
	 */
	constexpr DynamicVerified(T& wrapped, const char* paramName,
	                          XrResult errorCode)
	    : DynamicVerifiedOrNull<T>(std::addressof(wrapped), paramName,
	                               errorCode)
	{}

	/*! "Private" constructor from the head of a chain, when we just want
	 * one member of the chain.
	 *
	 * @note Prefer calling xrtraits::getFromChain() instead of this
	 * constructor directly.
	 */
	template <typename U>
	constexpr DynamicVerified(U* param, const char* paramName,
	                          XrResult errorCode,
	                          detail::GetFromChain_t const& /*tag*/)
	    : DynamicVerified(
	          detail::getFromChainImpl<T>(param, paramName, errorCode))
	{
		Expects(param != nullptr);
	}

	/*! Conversion constructor between unrelated types deleted.
	 *
	 * This improves the clarity of error messages if you have a type
	 * mismatch - says you call an explicitly-deleted constructor, instead
	 * of listing all the possible constructors and why they aren't right.
	 *
	 * If you get an error on the line below, you're trying to pass
	 * DynamicVerified<U> as DynamicVerified<T> where T and U
	 * are different types. That's not allowed because it doesn't make
	 * sense in the OpenXR object model.
	 */
	template <typename U>
	DynamicVerified(DynamicVerified<U> const&) = delete;

	//! Copy-constructible.
	constexpr DynamicVerified(DynamicVerified const&) noexcept = default;

	//! Copy-assignable.
	constexpr DynamicVerified&
	operator=(DynamicVerified const&) noexcept = default;

	//! Move-constructible.
	constexpr DynamicVerified(DynamicVerified&&) noexcept = default;

	//! Move-assignable.
	constexpr DynamicVerified&
	operator=(DynamicVerified&&) noexcept = default;

	// Default destructor
	~DynamicVerified() = default;

	//! Not permitted to construct from nullptr
	DynamicVerified(std::nullptr_t) = delete;

	//! Retrieve the pointer.
	constexpr T* get() const XRTRAITS_NOEXCEPT_IF_NOT_RECHECKING
	{
#ifdef XRTRAITS_DYNAMIC_VERIFIED_ALWAYS_RECHECK
		if (Base::uncheckedGet()->type != traits::xr_type_tag_v<T>) {
			throw exceptions::xr_logic_error(
			    "Something changed the .type member of an OpenXR "
			    "struct after a DynamicVerified wrapper was "
			    "created for it.");
		}
#else
		GSL_ASSUME(Base::uncheckedGet()->type ==
		           traits::xr_type_tag_v<T>);
#endif // XRTRAITS_DYNAMIC_VERIFIED_ALWAYS_RECHECK
		return Base::uncheckedGet();
	}

	/*! "Smart Pointer" operator: for `p->memberName` as if this was just a
	 * raw pointer to the wrapped struct.
	 */
	constexpr T* operator->() const XRTRAITS_NOEXCEPT_IF_NOT_RECHECKING
	{
		return get();
	}

	/*! Dereference operator: for `*p` as if this was just a raw pointer
	 * to the wrapped struct.
	 */
	constexpr T& operator*() const XRTRAITS_NOEXCEPT_IF_NOT_RECHECKING
	{
		return *get();
	}

	/*! Conversion directly from Initialized<T> to DynamicVerified<T> uses
	 * the private constructor.
	 */
	template <typename U> friend struct Initialized;

private:
	/*! Private constructor that skips verification of dynamic type, for
	 * use within this file's implementation only when we've already done
	 * this verification.
	 */
	explicit constexpr DynamicVerified(T& wrapped) noexcept
	    : DynamicVerifiedOrNull<T>(std::addressof(wrapped))
	{
		GSL_ASSUME(wrapped.type == traits::xr_type_tag_v<T>);
	}
};

/*! Equality comparison operator between DynamicVerified and nullptr - always
 * false.
 *
 * @relates DynamicVerified
 */
template <typename T>
inline constexpr bool operator==(DynamicVerified<T> const& /*unused*/,
                                 std::nullptr_t /*unused*/)
{
	return false;
}

/*! Equality comparison operator between DynamicVerified and nullptr - always
 * false.
 *
 * @relates DynamicVerified
 */
template <typename T>
inline constexpr bool operator==(std::nullptr_t /*unused*/,
                                 DynamicVerified<T> const& /*unused*/)
{
	return false;
}

/*! Inequality comparison operator between DynamicVerified and nullptr - always
 * true.
 *
 * @relates DynamicVerified
 */
template <typename T>
inline constexpr bool operator!=(DynamicVerified<T> const& /*unused*/,
                                 std::nullptr_t /*unused*/)
{
	return true;
}

/*! Inequality comparison operator between DynamicVerified and nullptr - always
 * true.
 *
 * @relates DynamicVerified
 */
template <typename T>
inline constexpr bool operator!=(std::nullptr_t /*unused*/,
                                 DynamicVerified<T> const& /*unused*/)
{
	return true;
}

// No more doc comments below - most of them have been moved to the top of the
// file along with their forward declarations.

template <typename T>
XRTRAITS_NODISCARD inline constexpr DynamicVerifiedOrNull<T>
verifyDynamicTypeOrNull(T* const& param, const char* paramName,
                        XrResult errorCode)
{
	static_assert(traits::has_xr_type_tag_v<T>,
	              "Can only use verifyDynamicTypeOrNull on types with an "
	              "associated XrStructureType enum value.");
	if (nullptr == param) {
		return {nullptr};
	}
	return {param, paramName, errorCode};
}

template <typename T>
XRTRAITS_NODISCARD inline constexpr DynamicVerified<T>
verifyDynamicType(T* param, const char* paramName, XrResult errorCode)
{
	static_assert(traits::has_xr_type_tag_v<T>,
	              "Can only use verifyDynamicType() on types with an "
	              "associated XrStructureType enum value.");
	if (nullptr == param) {
		throw exceptions::type_error(
		    paramName, nullptr, traits::xr_type_tag_v<T>, errorCode);
	}
	return {*param, paramName, errorCode};
}

template <typename T>
XRTRAITS_NODISCARD inline constexpr DynamicVerified<T>
verifyDynamicType(T& param, const char* paramName, XrResult errorCode)
{
	static_assert(traits::has_xr_type_tag_v<T>,
	              "Can only use verifyDynamicType() on types with an "
	              "associated XrStructureType enum value.");
	return {param, paramName, errorCode};
}

template <typename T, typename U>
XRTRAITS_NODISCARD inline constexpr DynamicVerified<T>
required_verified_cast(U* const& param, const char* paramName,
                       XrResult errorCode)
{
	static_assert(traits::has_xr_type_tag_v<T>,
	              "Can only use required_verified_cast() with type params "
	              "that have an associated XrStructureType enum value.");
	static_assert(!is_const_v<U> || is_const_v<T>,
	              "Can't use required_verify_cast() to cast away const.");
	static_assert(!is_pointer_v<T>,
	              "Pass just the target type, not a pointer, "
	              "as the type param of required_verified_cast()");
	static_assert(!is_reference_v<T>,
	              "Pass just the target type, not a reference, "
	              "as the type param of required_verified_cast()");

	if (nullptr == param) {
		throw exceptions::type_error(
		    paramName, nullptr, traits::xr_type_tag_v<T>, errorCode);
	}
	auto* castResult = casts::xr_tagged_dynamic_cast<T*>(param);
	if (castResult == nullptr) {
		throw exceptions::type_error(paramName, param->type,
		                             traits::xr_type_tag_v<T>,
		                             errorCode);
	}
	return {*castResult, paramName, errorCode};
}

template <typename T, typename U>
XRTRAITS_NODISCARD inline constexpr DynamicVerified<T>
required_verified_cast(U& param, const char* paramName, XrResult errorCode)
{
	static_assert(traits::has_xr_type_tag_v<T>,
	              "Can only use required_verified_cast() with type params "
	              "that have an associated XrStructureType enum value.");
	static_assert(!is_const_v<U> || is_const_v<T>,
	              "Can't use required_verify_cast() to cast away const.");
	static_assert(!is_pointer_v<T>,
	              "Pass just the target type, not a pointer, "
	              "as the type param of required_verified_cast()");
	static_assert(!is_reference_v<T>,
	              "Pass just the target type, not a reference, "
	              "as the type param of required_verified_cast()");

	auto* castResult =
	    casts::xr_tagged_dynamic_cast<T*>(std::addressof(param));
	if (castResult == nullptr) {
		throw exceptions::type_error(
		    paramName, param.type, traits::xr_type_tag_v<T>, errorCode);
	}
	return {*castResult, paramName, errorCode};
}

template <typename T, typename U>
XRTRAITS_NODISCARD inline constexpr DynamicVerifiedOrNull<T>
required_verified_or_null_cast(U* param, const char* paramName,
                               XrResult errorCode)
{
	static_assert(traits::has_xr_type_tag_v<T>,
	              "Can only use required_verified_cast() with type params "
	              "that have an associated XrStructureType enum value.");
	static_assert(
	    !is_const_v<U> || is_const_v<T>,
	    "Can't use required_verified_or_null_cast() to cast away const.");
	if (nullptr == param) {
		return {nullptr};
	}
	auto* castResult = casts::xr_tagged_dynamic_cast<T*>(param);
	if (castResult == nullptr) {
		throw exceptions::type_error(paramName, param->type,
		                             traits::xr_type_tag_v<T>,
		                             errorCode);
	}
	return {*castResult, paramName, errorCode};
}

// Doxygen gets confused about this function, warning unnecessarily, even though
// it does accurately pick up the doc comment on the formward declaration above.
#ifndef XRTRAITS_DOXYGEN

template <typename T>
XRTRAITS_NODISCARD inline constexpr DynamicVerified<T>
ensureNotNull(DynamicVerifiedOrNull<T> const& param, const char* paramName,
              XrResult errorCode)
{
	static_assert(traits::has_xr_type_tag_v<T>,
	              "Can only use ensureNotNull() on types with an "
	              "associated XrStructureType enum value.");
	if (nullptr == param) {
		throw exceptions::type_error(
		    paramName, nullptr, traits::xr_type_tag_v<T>, errorCode);
	}
	return {*param, paramName, errorCode};
}

#endif // !XRTRAITS_DOXYGEN

template <typename TargetType, typename SourceType>
XRTRAITS_NODISCARD inline constexpr DynamicVerified<TargetType>
getFromChain(SourceType* param, const char* paramName, XrResult errorCode)
{
	static_assert(traits::is_xr_tagged_type_v<SourceType>,
	              "Can only use getFromChain() on types known to have type "
	              "and next members.");
	static_assert(!is_pointer_v<TargetType>,
	              "Type parameter to getFromChain() should just be the "
	              "structure type, not a pointer type.");
	static_assert(!is_reference_v<TargetType>,
	              "Type parameter to getFromChain() should just be the "
	              "structure type, not a reference type.");

	return {param, paramName, errorCode, detail::get_from_chain};
}

template <typename TargetType, typename SourceType>
XRTRAITS_NODISCARD inline constexpr std::enable_if_t<
    traits::is_xr_tagged_type_v<SourceType>, DynamicVerified<TargetType>>
getFromChain(SourceType& param, const char* paramName, XrResult errorCode)
{
	static_assert(traits::is_xr_tagged_type_v<SourceType>,
	              "Can only use getFromChain() on types known to have type "
	              "and next members.");
	static_assert(!is_pointer_v<TargetType>,
	              "Type parameter to getFromChain() should just be the "
	              "structure type, not a pointer type.");
	static_assert(!is_reference_v<TargetType>,
	              "Type parameter to getFromChain() should just be the "
	              "structure type, not a reference type.");

	return {std::addressof(param), paramName, errorCode,
	        detail::get_from_chain};
}

template <typename TargetType, typename SourceType>
XRTRAITS_NODISCARD inline constexpr DynamicVerified<TargetType>
getFromChain(DynamicVerified<SourceType> const& param, const char* paramName,
             XrResult errorCode)
{
	static_assert(!is_pointer_v<TargetType>,
	              "Type parameter to getFromChain() should just be the "
	              "structure type, not a pointer type.");
	static_assert(!is_reference_v<TargetType>,
	              "Type parameter to getFromChain() should just be the "
	              "structure type, not a reference type.");
	return {param.get(), paramName, errorCode, detail::get_from_chain};
}

template <typename TargetType, typename SourceType>
XRTRAITS_NODISCARD inline constexpr DynamicVerifiedOrNull<TargetType>
tryGetFromChain(SourceType* param) noexcept
{
	static_assert(traits::is_xr_tagged_type_v<SourceType>,
	              "Can only use tryGetFromChain() on types known to have "
	              "type and next members.");
	static_assert(!is_pointer_v<TargetType>,
	              "Type parameter to tryGetFromChain() should just be the "
	              "structure type, not a pointer type.");
	static_assert(!is_reference_v<TargetType>,
	              "Type parameter to tryGetFromChain() should just be the "
	              "structure type, not a reference type.");

	return {param, detail::get_from_chain};
}

template <typename TargetType, typename SourceType>
XRTRAITS_NODISCARD inline constexpr std::enable_if_t<
    traits::is_xr_tagged_type_v<SourceType>, DynamicVerifiedOrNull<TargetType>>
tryGetFromChain(SourceType& param) noexcept
{
	static_assert(traits::is_xr_tagged_type_v<SourceType>,
	              "Can only use tryGetFromChain() on types known to have "
	              "type and next members.");
	static_assert(!is_pointer_v<TargetType>,
	              "Type parameter to tryGetFromChain() should just be the "
	              "structure type, not a pointer type.");
	static_assert(!is_reference_v<TargetType>,
	              "Type parameter to tryGetFromChain() should just be the "
	              "structure type, not a reference type.");

	return {std::addressof(param), detail::get_from_chain};
}

template <typename TargetType, typename SourceType>
XRTRAITS_NODISCARD inline constexpr DynamicVerifiedOrNull<TargetType>
tryGetFromChain(DynamicVerifiedOrNull<SourceType> const& param) noexcept
{
	static_assert(!is_pointer_v<TargetType>,
	              "Type parameter to tryGetFromChain() should just be the "
	              "structure type, not a pointer type.");
	static_assert(!is_reference_v<TargetType>,
	              "Type parameter to tryGetFromChain() should just be the "
	              "structure type, not a reference type.");
	return {param.get(), detail::get_from_chain};
}
#if 0
template <typename T>
class DynamicVerifiedSpan : public span<T, gsl::dynamic_extent>
{
public:
	//! Underlying span type.
	using base = span<T, gsl::dynamic_extent>;

	//! Type used for size
	using size_type = typename base::size_type;

	static_assert(traits::has_xr_type_tag_v<T>,
	              "Can only use DynamicVerifiedSpan on types with an "
	              "associated XrStructureType enum value.");

	/*! Constructor - performs type checking of all elements in the array.
	 *
	 * @note Prefer make_dynamic_verified_span() to calling this directly.
	 */
	constexpr DynamicVerifiedSpan(T* pointer, size_type sz,
	                              const char* paramName, XrResult errorCode)
	    : span<T, gsl::dynamic_extent>(pointer, sz)
	{
		if (pointer == nullptr) {
			throw exceptions::type_error(paramName, nullptr,
			                             traits::xr_type_tag_v<T>,
			                             sz, errorCode);
		}
		for (size_type i = 0; i < sz; ++i) {
			const auto t = (*this)[i].type;
			if (t != traits::xr_type_tag_v<T>) {
				throw exceptions::type_error(
				    paramName, t, traits::xr_type_tag_v<T>, i,
				    errorCode);
			}
		}
	}
};

template <typename T>
XRTRAITS_NODISCARD inline constexpr DynamicVerifiedSpan<T>
make_dynamic_verified_span(T* pointer, std::ptrdiff_t size,
                           const char* paramName, XrResult errorCode)
{
	static_assert(
	    traits::has_xr_type_tag_v<T>,
	    "Can only use make_dynamic_verified_span on types with an "
	    "associated XrStructureType enum value.");
	return {pointer, size, paramName, errorCode};
}
} // namespace xrtraits

#endif

} // namespace xrtraits

#undef XRTRAITS_NOEXCEPT_IF_NOT_RECHECKING

#endif // defined(XRTRAITS_HAVE_EXCEPTIONS) &&
       // defined(XRTRAITS_HAVE_CONSTEXPR_IF)
