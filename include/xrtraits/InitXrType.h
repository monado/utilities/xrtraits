// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header providing a variety of ways to create or initialize an OpenXR
 * "tagged struct".
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "DynamicVerified.h"
#include "traits/APITraits.h"

// Library Includes
#include <openxr/openxr.h>

// Standard Includes
#include <cstring>
#include <type_traits>
#include <vector>

namespace xrtraits {

/*!
 * @defgroup Initialization Initializing OpenXR Structs
 *
 * @brief Wrapper types and functions to build known-good OpenXR structs.
 */

/*! Given a pointer to some storage to use as an OpenXR tagged type, zeros the
 * storage and sets the type member.
 *
 * Overload that automatically deduces the type from the passed pointer.
 *
 * @ingroup Initialization
 */
template <typename T> inline T* initXrType(T* storage)
{
	static_assert(traits::has_xr_type_tag_v<T>,
	              "Can only call initXrType to initialize a type with a "
	              "known OpenXR type tag");
	static_assert(
	    !is_const_v<T>,
	    "Doesn't make sense to pass a reference to const to initXrType()");
	std::memset(storage, 0, sizeof(T));
	storage->type = traits::xr_type_tag_v<T>;
	return storage;
}

/*! Given an OpenXR tagged type and a pointer to sufficient storage, zeros the
 * storage and sets the type member.
 *
 * @ingroup Initialization
 */
template <typename T> inline T* initXrType(void* storage)
{
	// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
	return initXrType(reinterpret_cast<T*>(storage));
}

/*! Given a reference to some storage to use as an OpenXR tagged type, zeros the
 * storage and sets the type member.
 *
 * Overload that automatically deduces the type from the passed reference.
 *
 * @ingroup Initialization
 */
template <typename T> inline T* initXrType(T& storage)
{
	static_assert(
	    !is_const_v<T>,
	    "Doesn't make sense to pass a reference to const to initXrType()");
	return initXrType(&storage);
}

#ifndef XRTRAITS_DOXYGEN

namespace detail {
	//! Wraps initXrType() to avoid needing constexpr-if
	template <typename T, typename = void> struct ConditionalInitXrType
	{
		static void apply(T&) {}
	};
	template <typename T>
	struct ConditionalInitXrType<
	    T, std::enable_if_t<traits::has_xr_type_tag_v<T>>>
	{
		static void apply(T& storage) { initXrType(&storage); }
	};
} // namespace detail

#endif // !XRTRAITS_DOXYGEN

/*! Creates, initializes, and returns an instance of an OpenXR tagged type.
 *
 * Sets the type member and zeros the rest of the storage.
 *
 * @ingroup Initialization
 */
template <typename T> inline T make_zeroed()
{
	T ret{};
	detail::ConditionalInitXrType<T>::apply(ret);
	return ret;
}

/*! Creates, initializes, and returns a vector containing the given number of
 * empty instances of an OpenXR tagged type.
 *
 * @ingroup Initialization
 */
template <typename T> inline std::vector<T> make_zeroed_vector(size_t n)
{
	return std::vector<T>{n, make_zeroed<T>()};
}

#ifndef XRTRAITS_DOXYGEN
template <typename T> struct Initialized;
namespace detail {
	template <typename T>
	struct is_chainable_struct_oracle : std::false_type
	{
	};
#ifdef XRTRAITS_HAVE_DYNAMIC_VERIFIED
	template <typename U>
	struct is_chainable_struct_oracle<DynamicVerified<U>> : std::true_type
	{
	};
#endif // XRTRAITS_HAVE_DYNAMIC_VERIFIED

	template <typename U>
	struct is_chainable_struct_oracle<Initialized<U>> : std::true_type
	{
	};
	template <typename T>
	constexpr bool is_chainable_struct_v = is_chainable_struct_oracle<
	    std::remove_cv_t<std::remove_reference_t<T>>>::value;
} // namespace detail
#endif // !XRTRAITS_DOXYGEN

#if defined(__GCC__)
// This warning will trigger for most uses of Initialized, because we're
// (intentionally, in most cases) only providing initializers for the type and
// next.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

/*! Wrapper for stack-allocated OpenXR tagged structs that auto-initializes the
 * type member correctly.
 *
 * Inherits from the type parameter, so this object is the same size as the
 * OpenXR struct and exposes the same members, etc...
 *
 * @tparam T an OpenXR tagged type with known type tag.
 *
 * @ingroup Initialization
 */
template <typename T> struct Initialized : T
{
	static_assert(traits::has_xr_type_tag_v<T>,
	              "Can only use Initialized<T> with a type with a known "
	              "OpenXR type tag");
	static_assert(!is_pointer_v<T>,
	              "Can't use Initialized<T> with a pointer type - just use "
	              "the bare type, we are stack allocating.");
	static_assert(!is_reference_v<T>,
	              "Can't use Initialized<T> with a reference type.");
	static_assert(!is_const_v<T>,
	              "Can't use Initialized<T> with a const type - doesn't "
	              "make any sense.");

	//! The OpenXR structure type.
	using xr_type = T;

	//! The OpenXR structure type (const).
	using const_xr_type = std::add_const_t<T>;

	//! Get pointer to OpenXR type.
	constexpr xr_type* get() noexcept { return this; }

	//! Get pointer to OpenXR type (const overload).
	constexpr const_xr_type* get() const noexcept { return this; }

	/*! Can't access the underlying struct's type member by design.
	 *
	 * Naming this here and explicitly deleting it makes it harder to
	 * accidentally mess up an OpenXR struct's `type` member (unless you
	 * really try hard). If you get an error on this line, it's because
	 * you're trying to manually look at or modify type, which shouldn't be
	 * done.
	 */
	void type() const = delete;

	/*! Can't access the underlying struct's next member by design.
	 *
	 * Naming this here and explicitly deleting it makes it harder to
	 * accidentally mess up an OpenXR struct's `next` member (unless you
	 * really try hard). If you get an error on this line, it's because
	 * you're trying to manually look at or modify type, which shouldn't be
	 * done.
	 */
	void next() const = delete;

	//! Default constructor - initializes OpenXR struct's type member.
	constexpr Initialized() noexcept : T({})
	{
		// Not using initializer because that would produce "missing
		// initializer for member" warnings.
		static_cast<T*>(this)->type = traits::xr_type_tag_v<T>;
	}

#ifdef XRTRAITS_HAVE_DYNAMIC_VERIFIED
	/*! Constructor to reference a chained struct (passed as
	 * DynamicVerified<U>).
	 *
	 * Initializes OpenXR struct's type member, and the
	 * next member to the given structure's address. Any additional
	 * parameters are forwarded as member initializers to the underlying
	 * struct.
	 */
	template <typename U, typename... Args>
	explicit constexpr Initialized(
	    DynamicVerified<U> nextStruct,
	    Args... a) noexcept(noexcept(nextStruct.get()))
	    : T({traits::xr_type_tag_v<T>, nextStruct.get(),
	         std::forward<Args>(a)...})
	{}
#endif // XRTRAITS_HAVE_DYNAMIC_VERIFIED

	/*! Constructor to reference a chained struct (passed as
	 * Initialized<U>).
	 *
	 * Initializes OpenXR struct's type member, and the
	 * next member to the given structure's address. Any additional
	 * parameters are forwarded as member initializers to the underlying
	 * struct.
	 */
	template <typename U, typename... Args>
	explicit constexpr Initialized(Initialized<U> const& nextStruct,
	                               Args... a) noexcept
	    : T({traits::xr_type_tag_v<T>, nextStruct.get(),
	         std::forward<Args>(a)...})
	{
		static_assert(
		    sizeof...(a) != 0 ||
		        !is_same_v<std::remove_cv_t<T>, std::remove_cv_t<U>>,
		    "Can't copy Initialized<T> structures.");
	}

	/*! Constructor to reference a possibly non-const chained struct (passed
	 * as Initialized<U>).
	 *
	 * Initializes OpenXR struct's type member, and the
	 * next member to the given structure's address. Any additional
	 * parameters are forwarded as member initializers to the underlying
	 * struct.
	 */
	template <typename U, typename... Args>
	explicit constexpr Initialized(Initialized<U>& nextStruct,
	                               Args... a) noexcept
	    : T({traits::xr_type_tag_v<T>, nextStruct.get(),
	         std::forward<Args>(a)...})
	{
		static_assert(
		    !is_same_v<std::remove_cv_t<T>, std::remove_cv_t<U>>,
		    "Can't copy Initialized<T> "
		    "structures.");
	}
	/*! Constructor that forwards initializers and has no chained struct.
	 *
	 * Initializes OpenXR struct's type member, and forwards the rest of the
	 * arguments on as initializers.
	 */
	template <
	    typename A1, typename... Args,
	    typename = std::enable_if_t<!detail::is_chainable_struct_v<A1>>>
	explicit constexpr Initialized(A1&& a1, Args... a) noexcept
	    : T({traits::xr_type_tag_v<T>, nullptr, std::forward<A1>(a1),
	         std::forward<Args>(a)...})
	{}

	/*! Default move-constructor - mainly for assignment-style
	 * initialization.
	 */
	constexpr Initialized(Initialized&&) noexcept = default;

	// Non-copy-constructible
	constexpr Initialized(Initialized const&) = delete;
	// Non-copy-assignable
	constexpr Initialized& operator=(Initialized const&) = delete;
	// Non-move-assignable
	constexpr Initialized& operator=(Initialized&&) = delete;

	// Default destructor
	~Initialized() = default;

#ifdef XRTRAITS_HAVE_DYNAMIC_VERIFIED
	//! The DynamicVerified class associated with this type as non-const.
	using verified_type = DynamicVerified<xr_type>;

	//! The DynamicVerified class associated with this type as const.
	using const_verified_type = DynamicVerified<std::add_const_t<xr_type>>;

	/*! Manual conversion to DynamicVerified<T>.
	 *
	 * By design, this class ensures the dynamic type is correct, so we get
	 * to skip verification and use the private constructor.
	 */
	constexpr verified_type asVerified() { return verified_type{*get()}; }

	/*! Manual conversion to DynamicVerified<const T> (const overload).
	 *
	 * By design, this class ensures the dynamic type is correct, so we get
	 * to skip verification and use the private constructor.
	 */
	constexpr const_verified_type asVerified() const
	{
		return const_verified_type{*get()};
	}

	/*! Manual conversion to DynamicVerified<const T>.
	 *
	 * By design, this class ensures the dynamic type is correct, so we get
	 * to skip verification and use the private constructor.
	 */
	constexpr const_verified_type asVerifiedConst() const
	{
		return asVerified();
	}

	/*! Conversion operator to DynamicVerified<T>.
	 *
	 * By design, this class ensures the dynamic type is correct, so we get
	 * to skip verification and use the private constructor.
	 */
	constexpr operator verified_type() { return asVerified(); }

	/*! Conversion operator to DynamicVerified<const T>.
	 *
	 * By design, this class ensures the dynamic type is correct, so we get
	 * to skip verification and use the private constructor.
	 */
	constexpr operator const_verified_type() const { return asVerified(); }
#endif // XRTRAITS_HAVE_DYNAMIC_VERIFIED
};

#if defined(__GCC__)
#pragma GCC diagnostic pop
#endif

} // namespace xrtraits
