// Copyright 2018-2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Bits of Doxygen documentation that doesn't correspond neatly to a
 * single source file - not actually compiled.
 *
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

/*!
 * @namespace xrtraits
 *
 * @brief Main namespace for these C++ OpenXR utilities.
 */

/*!
 * @namespace xrtraits::exceptions
 * @brief Namespace for custom exception types that may be thrown internal to
 * the utilities.
 *
 * Note that these declarations (as well as any possibly-throwing functionality
 * elsewhere in the library) are only available if `XRTRAITS_USE_EXCEPTIONS` is
 * defined and the source files compiled. Without that flag, the source files
 * are not needed, and exceptions are not explicitly thrown.
 */

/*!
 * @namespace xrtraits::traits
 * @brief C++11/14/17 "type traits" for manipulating and reasoning about types
 * at compile-time.
 *
 * Also includes a few other `constexpr` (compile time or run time) functions
 * based on generated code.
 */

/*!
 * @namespace xrtraits::casts
 * @brief Casts for OpenXR tagged types, as well as utilities for traversing a
 * `next` chain.
 *
 * **The non-chained "cast-like" functions:**
 *
 * In both xr_tagged_dynamic_cast() and xr_tagged_risky_cast(), if the target
 * type parameter you supply is a pointer type, a null pointer input or mismatch
 * of the `type` member with your target type will result in the cast returning
 * `nullptr` if your target is a pointer type, or throwing
 * xrtraits::exceptions::bad_tagged_cast if your target is a reference type.
 * Choose whether to request a pointer or reference type as target based on your
 * confidence that the input is the right type; remember that exceptions are for
 * exceptional cases. This behavior intentionally resembles the standard
 * dynamic_cast.
 *
 * - xrtraits::casts::xr_tagged_dynamic_cast() -
 *   Provide a target type (pointer or reference) and a typed pointer or
 *   reference. e.g.
 *   ```cpp
 *   XrEventDataBaseHeader * someEventPtr = something;
 *   auto typedPerfEventPtr =
 *       xr_tagged_dynamic_cast<XrEventDataPerfSettingsEXT*>(someEventPtr);
 *   if (typedPerfEventPtr) { doSomethingWith(typedPerfEventPtr); }
 *   ```
 * - xrtraits::casts::xr_tagged_risky_cast() -
 *   Like xrtraits::casts::xr_tagged_dynamic_cast() but riskier: Used when you
 *   only have `void *` or `const void *`, but you can confidently say that the
 *   pointed object is an OpenXR tagged type. (It relies on this assumption!)
 *   Does not take references as input parameters - references to void make
 *   little sense.
 *
 * If these are used wrong, there are static assertions that will try to provide
 * a helpful error message.
 */

/*!
 * @defgroup TypeVerification Dynamic Typing Verification
 *
 * @brief High-level functionality to interact with OpenXR "tagged" types in
 * typesafe, type-checked ways.
 *
 * Mostly useful in code accepting an OpenXR structure from other,
 * possibly-buggy code.
 */

/*!
 * @defgroup Casts Cast-Like Functions
 *
 * @brief Casts for OpenXR tagged types, as well as utilities for traversing a
 * `next` chain.
 */
