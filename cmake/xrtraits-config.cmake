# Copyright 2019, Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0

# @THIS_IS_THE_INPUT_FILE_EDIT_THIS_ONE@

set(XRTRAITS_IS_BUILD_TREE @XRTRAITS_IS_BUILD_TREE@)

include("${CMAKE_CURRENT_LIST_DIR}/xrtraits-config-targets.cmake")

if(NOT XRTRAITS_IS_BUILD_TREE)
    if(NOT TARGET OpenXR::Headers)
        list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR})
        include(CMakeFindDependencyMacro)
        find_dependency(OpenXR COMPONENT headers)
    endif()
    target_link_libraries(xrtraits::xrtraits_openxr INTERFACE OpenXR::Headers)
endif()
